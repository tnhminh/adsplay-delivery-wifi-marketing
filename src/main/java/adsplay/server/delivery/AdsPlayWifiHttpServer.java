package adsplay.server.delivery;

import adsplay.delivery.redis.cache.RedisCache;
import rfx.core.util.StringUtil;

public class AdsPlayWifiHttpServer {
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            args = new String[] { "127.0.0.1", "9999" };
        }
        String host = args[0];
        int port = StringUtil.safeParseInt(args[1]);
        System.setProperty("vertx.disableFileCPResolving", "true");

        // Init redis cache first
        // DeliveryRedisCache.updateCachingTask();

        // Adding new Instance here
        // AdDeliveryWorker.startNewInstance(host, port, new RoutingHandler());
        
        RedisCache.init();
        DeliveryWorker.startNewInstance(host, port, new WifiRoutingHandler());
    }
}
