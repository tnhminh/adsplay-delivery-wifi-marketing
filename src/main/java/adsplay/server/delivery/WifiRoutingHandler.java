package adsplay.server.delivery;

import static io.vertx.core.http.HttpHeaders.CONNECTION;
import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpStatus;

import com.adsplay.graylog.enumeration.GrayLevel;
import com.adsplay.graylog.handler.GrayLogHandler;
import com.adsplay.sentry.handler.SentryHanlder;

import adsplay.common.BaseHttpLogHandler;
import adsplay.delivery.handler.AdsplayWifiHandler;
import adsplay.delivery.properties.DeliveryConfiguration;
import adsplay.delivery.properties.MessageProperties;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.net.SocketAddress;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;
import server.http.util.HttpTrackingUtil;

public class WifiRoutingHandler extends BaseHttpLogHandler {

    private static final int FIVE_MINUTES = 300000;
    public static final String ADSPLAY_NET = "adsplay.net";
    public static final String APLUUID = "apluuid";

    // delivery prefix URI
    public static final String DELIVERY_PREFIX = MessageProperties.getProperty("general.delivery.fptplay.prefix", "");

    public static final String POWERED_BY = MessageProperties.getProperty("general.powered.by", "");
    public static final String ADS_PLAY_VERSION = MessageProperties.getProperty("general.ads.play.version", "");

    public static final String HTTP = "http://";
    private static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();
    public static final String BASE_DOMAIN = WORKER_CONFIGS.getCustomConfig("baseServerDomain");
    public static final String LOG1_DOMAIN = WORKER_CONFIGS.getCustomConfig("log1ServerDomain");
    public static final String LOG2_DOMAIN = WORKER_CONFIGS.getCustomConfig("log2ServerDomain");
    public static final String LOG3_DOMAIN = WORKER_CONFIGS.getCustomConfig("log3ServerDomain");
    public static final String LOG4_DOMAIN = WORKER_CONFIGS.getCustomConfig("log4ServerDomain");
    
    private static Timer timer = new Timer();
    
    static {
        timer.scheduleAtFixedRate(new TimerTask() {
            
            @Override
            public void run() {
                // Run Garbage Collector
                System.gc();
            }
        }, 500, FIVE_MINUTES);
    }
    
    public WifiRoutingHandler() {
    }

    @Override
    public void handle(HttpServerRequest request) {

    }

    static final String unknown = "unknown";

    public static String getRemoteIP(HttpServerRequest request) {
        String ipAddress = request.headers().get("X-Forwarded-For");
        if (!StringUtil.isNullOrEmpty(ipAddress) && !unknown.equalsIgnoreCase(ipAddress)) {
            // LogUtil.dumpToFileIpLog(ipAddress);
            String[] toks = ipAddress.split(",");
            int len = toks.length;
            if (len > 1) {
                ipAddress = toks[len - 1];
            } else {
                return ipAddress;
            }
        } else {
            ipAddress = getIpAsString(request.remoteAddress());
        }
        LogUtil.i(WifiRoutingHandler.class, "final ipAddress: " + ipAddress, true);
        return ipAddress;
    }

    public static String getIpAsString(SocketAddress address) {
        try {
            if (address instanceof InetSocketAddress) {
                return ((InetSocketAddress) address).getAddress().getHostAddress();
            }
            return address.toString().split("/")[1].split(":")[0];
        } catch (Throwable e) {
            LogUtil.error(WifiRoutingHandler.class, "getIpAsString", "Failure when getIpAsString with address:" + address.toString());
        }
        return "0.0.0.0";
    }

    private static ExecutorService blockingJobService = Executors
            .newFixedThreadPool(Integer.valueOf(DeliveryConfiguration.getProperty("job.worker.pool.size", "100")));

    public static void runWorker(String uri, MultiMap params, List<String> pmIds, Future<String> futureExecuteBlock, HttpServerRequest request,
            Map<String, Object> additionalInfos) {

        // Do the blocking operation in here
        String rsText = "";

        additionalInfos.put("delivery.uri", uri);

        String remoteIp = getRemoteIP(request);

        long startTime = System.currentTimeMillis();

        java.util.concurrent.Future<String> futureTask = blockingJobService.submit(new Callable<String>() {

            @Override
            public String call() throws Exception {
                return execute(uri, params, pmIds, additionalInfos, remoteIp);
            }
        });
        try {
            rsText = futureTask.get(1000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            if (e instanceof TimeoutException) {
                LogUtil.error(WifiRoutingHandler.class, "runWorker", "Timed out worker thread with uri:" + uri);
                rsText = "{\"message\":\"408 Request Timeout\", \"status\":\"Failure\"}";
            } else {
                LogUtil.error(WifiRoutingHandler.class, "runWorker",
                        "InterruptedException/ExecutionException when execute delivery task, uri:" + uri);
                rsText = "{\"message\":\"500 Internal Server Error\", \"status\":\"" + e.getMessage() + "\"}";
            }
            LogUtil.error(e);
        } finally {
            futureTask.cancel(true);
        }
        long executionTime = System.currentTimeMillis() - startTime;

        additionalInfos.put("delivery.execution.time.with.timed.out", executionTime);
        additionalInfos.put("delivery.result.isEmpty", Boolean.valueOf(rsText.isEmpty()).toString());

        futureExecuteBlock.complete(rsText);
    }

    private static String execute(String uri, MultiMap params, List<String> pmIds, Map<String, Object> additionalInfos, String remoteIp) {

        long startTime = System.currentTimeMillis();

        String rsText = "";
        if (uri.startsWith(DELIVERY_PREFIX)) {
            return AdsplayWifiHandler.handle(params, "", pmIds, additionalInfos);
        }

        long executionTime = System.currentTimeMillis() - startTime;

        additionalInfos.put("delivery.execution.time", executionTime);
        additionalInfos.put("delivery.result.isEmpty", Boolean.valueOf(rsText.isEmpty()).toString());
        try {
            additionalInfos.put("delivery.server.addr", InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            LogUtil.error(WifiRoutingHandler.class, "runWorker", "UnknownHostException have already occured !!!");
        }

        return rsText;
    }

    public static void routingToHandler(HttpServerRequest request, Vertx vertxInstance) {

        Map<String, Object> additionalInfos = new HashMap<String, Object>();

        try {
            String uri = request.uri();

            if (uri.contains("favicon.ico")) {
                String responseStatus = "{\"message\":\"422 Unprocessable Entity\", \"status\":\"Placement param not found on this request\",  \"uri\":\""
                        + uri + "\"}";
                request.response().end(responseStatus);
                return;
            }

            LogUtil.i(WifiRoutingHandler.class, " \n ======Start====== \n RoutingHandler.URI: " + uri, true);
            MultiMap params = request.params();

            MultiMap rqHeader = request.headers();
            String origin = StringUtil.safeString(rqHeader.get("origin"), "");

            List<String> pmIds = params.getAll("placement_id");

            // Adding executeBlocking here

            // Adding executeBlocking here
            vertxInstance.<String>executeBlocking(future -> {
                WifiRoutingHandler.runWorker(uri, params, pmIds, future, request, additionalInfos);
            }, res -> {
                try {
                    if (res.succeeded()) {
                        HttpServerResponse response = request.response();
                        doResponse(uri, res.result(), pmIds, response, params, origin, additionalInfos);
                        LogUtil.i(WifiRoutingHandler.class, "Response result to client: " + uri + "\n===========END===========", true);
                    } else {
                        LogUtil.error(WifiRoutingHandler.class, "routingToHandler", "Failure when doing response to client !!!");
                        LogUtil.error(res.cause());
                        SentryHanlder.sendErrorToSentryLog("injectToPersistent", "minhtnh2@fpt.com.vn", null, null, "",
                                new Exception("Failure when doing response to client with uri:" + uri, res.cause()), false);
                        HttpServerResponse response = request.response();
                        doResponse(uri, "", pmIds, response, params, origin, additionalInfos);
                    }
                } catch (Exception e) {
                    String err = e.getMessage();
                    request.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
                } finally {
                    GrayLogHandler.buildMessageAndSend(GrayLevel.INFO, "[Request]-[Delivery]", "Request was sent for all type",
                            Thread.currentThread().getStackTrace()[1].getLineNumber(),
                            String.valueOf(Thread.currentThread().getStackTrace()[1].getClassName()), additionalInfos);

                    // Refresh mem
                    additionalInfos.clear();
                }
            });

        } catch (Throwable e) {
            String err = e.getMessage();
            request.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
        }
    }

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerResponse response, MultiMap params,
            String origin, Map<String, Object> additionalInfos) {
        MultiMap outHeaders = response.headers();
        outHeaders.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);
        outHeaders.set(POWERED_BY, ADS_PLAY_VERSION);
        // Keep here (don't remove)
        setCorsHeaders(outHeaders, pmIds, origin);

        if (rsText.contains("408 Request Timeout")) {
            response.setStatusCode(HttpStatus.SC_REQUEST_TIMEOUT).end(rsText);
            return;
        } else if (rsText.contains("500 Internal Server Error")) {
            response.setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(rsText);
            return;
        } else if (!rsText.isEmpty()) {
            response.end(rsText);
            return;
        } else {
            response.end("{\"message\":\"204 No Content\", \"status\":\"Data is empty\",  \"uri\":\"" + uri + "\"" + "}");
            return;
        }
    }

    public static final String GIF = "image/gif";
    public static final String HEADER_CONNECTION_CLOSE = "Close";
    public static final String BASE64_GIF_BLANK = "R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerRequest req) {

        Buffer buffer = Buffer.buffer(BASE64_GIF_BLANK);
        HttpServerResponse response = req.response();
        MultiMap headers = response.headers();
        headers.set(CONTENT_TYPE, GIF);
        headers.set(CONTENT_LENGTH, String.valueOf(buffer.length()));
        headers.set(CONNECTION, HEADER_CONNECTION_CLOSE);
        setCorsHeaders(headers);
        response.end(buffer);

    }

    private final static void setCorsHeaders(MultiMap headers) {
        headers.set("Access-Control-Allow-Origin", "*");
        headers.set("Access-Control-Allow-Credentials", "true");
        headers.set("Access-Control-Allow-Methods", "POST, GET");
        headers.set("Access-Control-Allow-Headers", "origin, content-type, accept, Set-Cookie");
    }
}
