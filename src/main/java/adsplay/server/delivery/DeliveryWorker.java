package adsplay.server.delivery;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import adsplay.delivery.properties.DeliveryConfiguration;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerRequest;
import rfx.core.stream.node.worker.BaseWorker;
import rfx.core.util.StringUtil;
import rfx.core.util.Utils;
import server.http.handler.BaseHttpHandler;

public class DeliveryWorker extends BaseWorker {

    public final long MAX_TIMEOUT_EVENT_LOOP = 1 * 1000 * 1000000L;

    public final long MAX_TIMEOUT_WORKER_THREAD = 1 * 1000 * 1000000L;

    final BaseHttpHandler httpHandler;
    static DeliveryWorker instance;

    protected DeliveryWorker(String name, BaseHttpHandler httpHandler) {
        super(name);
        this.httpHandler = httpHandler;
    }

    static String getName(String host, int port) {
        return DeliveryWorker.class.getSimpleName() + "_" + host + "_" + port;
    }

    /**
     * Create new AdDeliveryWorker instance with implemented httpHandler
     * 
     * @param host
     * @param port
     * @param httpHandler
     */
    public static void startNewInstance(String host, int port, BaseHttpHandler httpHandler) {
        instance = new DeliveryWorker(getName(host, port), httpHandler);
        instance.start(host, port);
    }

    @Override
    protected void onStartDone() {
        System.out.println("AdDeliveryWorker is loaded ...");
    }

    public static DeliveryWorker getInstance() {
        if (instance == null) {
            throw new IllegalAccessError("startNewInstance must called before getInstance");
        }
        return instance;
    }

    final protected void registerWorkerHttpHandler1(String host, int port, Handler<HttpServerRequest> handler) {
        HttpServer server = checkAndCreateHttpServer(host, port);
        if (server == null) {
            System.err.println("registerWorkerHttpHandler return NULL value");
            return;
        }
        server.requestHandler(handler).listen(port, host);
        registerWorkerNodeIntoCluster();
    }

    private HttpServer checkAndCreateHttpServer(String host, int port) {
        if (isAddressAlreadyInUse(host, port)) {
            System.err.println(host + ":" + port + " isAddressAlreadyInUse!");
            Utils.exitSystemAfterTimeout(200);
            return null;
        }
        try {
            this.publicHost = host;
            this.publicPort = port;

            // disable the creation of file-cache folders ".vertx"
            System.setProperty("vertx.disableFileCPResolving", "true");

            // refer http://vertx.io/manual.html#performance-tuning
            // DeploymentOptions options = new
            // DeploymentOptions().setWorker(true);
            VertxOptions options = new VertxOptions();
            options.setMaxEventLoopExecuteTime(StringUtil.safeParseLong(DeliveryConfiguration.getProperty("event.loop.max.timed.out"), MAX_TIMEOUT_EVENT_LOOP));
            options.setMaxWorkerExecuteTime(StringUtil.safeParseLong(DeliveryConfiguration.getProperty("worker.thread.max.timed.out"), MAX_TIMEOUT_WORKER_THREAD));

            // Adding blocked thread check interval with 100ms
            options.setBlockedThreadCheckInterval(StringUtil.safeParseInt(DeliveryConfiguration.getProperty("blocked.thread.check.interval"), 100));
            
            options.setWorkerPoolSize(StringUtil.safeParseInt(DeliveryConfiguration.getProperty("worker.pool.size"), 100));
            //options.setEventLoopPoolSize(5);
            vertxInstance = Vertx.vertx(options);

            HttpServerOptions httpOptions = new HttpServerOptions();
            httpOptions.setAcceptBacklog(10000).setUsePooledBuffers(true);
            httpOptions.setSendBufferSize(4 * 1024);
            httpOptions.setReceiveBufferSize(4 * 1024);
            httpServerInstance = vertxInstance.createHttpServer(httpOptions);

            return httpServerInstance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    ExecutorService executorService = Executors.newFixedThreadPool(StringUtil.safeParseInt(DeliveryConfiguration.getProperty("event.loop.pool.size"), 16));
    @Override
    public void start(String host, int port) {
        registerWorkerHttpHandler1(host, port, new Handler<HttpServerRequest>() {
            @Override
            public void handle(HttpServerRequest request) {
                executorService.submit(new Runnable() {
                    
                    @Override
                    public void run() {
                        WifiRoutingHandler.routingToHandler(request, vertxInstance);
                    }
                });
            }

        });
    }

}
