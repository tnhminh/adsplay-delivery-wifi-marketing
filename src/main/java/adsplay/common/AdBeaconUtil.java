package adsplay.common;

import rfx.core.util.DateTimeUtil;
import rfx.core.util.SecurityUtil;
import rfx.core.util.StringUtil;

public class AdBeaconUtil {

    public static String createBeaconValue(int adid, String uuid, int placementId, int campaignId, int flightId) {
        String rawBeacon = StringUtil.toString(uuid, "|", placementId, "|", adid, "|", campaignId, "|", flightId);
        return SecurityUtil.encryptBeaconValue(rawBeacon);
    }

    public static String getAdParams(int adid, String uuid, int placementId, int campaignId, int flightId, String buyType, int categoryId, int sourceProvider, String conId, String wKey) {
        StringBuilder adData = new StringBuilder();
        adData.append("&adId=").append(adid);
        adData.append("&campaignId=").append(campaignId);
        adData.append("&flightId=").append(flightId);
        adData.append("&placementId=").append(placementId);
        adData.append("&catId=").append(categoryId);
        adData.append("&sourceProvider=").append(sourceProvider);
        adData.append("&buyType=").append(buyType);
        adData.append("&conId=").append(conId);
        adData.append("&weightKey=").append(wKey);
        adData.append("&t=").append(DateTimeUtil.currentUnixTimestamp());
        return adData.toString();
    }
    
    public static String getAdParams(int adid, String uuid, int placementId, int campaignId, int flightId) {
        return getAdParams(adid, uuid, placementId, campaignId, flightId, "", 0, 0, "", "");
    }
}
