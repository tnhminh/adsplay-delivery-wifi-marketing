package adsplay.common.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DateRange {
    
    @Expose
    @SerializedName("from")
    private String from;
    
    @Expose
    @SerializedName("to")
    private String to;
    
    @Expose
    @SerializedName("hours")
    private List<Integer> hours;
    
    @Expose
    @SerializedName("day_weeks")
    private List<Integer> dateOfWeeks;

    public DateRange(String from, String to, List<Integer> hours, List<Integer> dateOfWeeks) {
        super();
        this.from = from;
        this.to = to;
        this.hours = hours;
        this.dateOfWeeks = dateOfWeeks;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public List<Integer> getHours() {
        return hours;
    }

    public void setHours(List<Integer> hours) {
        this.hours = hours;
    }

    public List<Integer> getDateOfWeeks() {
        return dateOfWeeks;
    }

    public void setDateOfWeeks(List<Integer> dateOfWeeks) {
        this.dateOfWeeks = dateOfWeeks;
    }

}
