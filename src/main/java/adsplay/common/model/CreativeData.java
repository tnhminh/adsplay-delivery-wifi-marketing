package adsplay.common.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreativeData {

    @Expose
    @SerializedName("creative_id")
    private int creativeId;

    @Expose
    @SerializedName("creative_name")
    private String creativeName;

    @Expose
    @SerializedName("source")
    private String sourceName;

    @Expose
    @SerializedName("landing_page")
    private String landingPage;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("width")
    private String width;

    @Expose
    @SerializedName("height")
    private String height;

    @Expose
    @SerializedName("campaign_id")
    private String campaignId;

    @Expose
    @SerializedName("creative_type")
    private String creativeType;

    @Expose
    @SerializedName("display_type")
    private String displayType;

    @Expose
    @SerializedName("flight_id")
    private String flightId;

    @Expose
    @SerializedName("buy_type")
    private String buyType;

    @Expose
    @SerializedName("booking_type")
    private String bookingType;

    @SerializedName("frequency")
    private String frequency;

    @SerializedName("weight")
    private Integer weight;

    @Expose
    @SerializedName("total_bookings")
    private int totalBookings;

    @Expose
    @SerializedName("daily_bookings")
    private int dailyBookings;

    @Expose
    @SerializedName("date_range")
    List<DateRange> dateRanges = new ArrayList<DateRange>();

    @Expose
    @SerializedName("publisher")
    List<PublisherData> publishers = new ArrayList<PublisherData>();

    @Expose
    @SerializedName("mac_address")
    private List<String> macAddress;

    public CreativeData(int creativeId, String creativeName, String sourceName, String landingPage, String type, String width,
            String height, String campaignId, String creativeType, String displayType, String flightId, String buyType, String bookingType,
            String frequency, Integer weight, int totalBookings, int dailyBookings, List<DateRange> dateRanges,
            List<PublisherData> publishers, List<String> macAddress) {
        super();
        this.creativeId = creativeId;
        this.creativeName = creativeName;
        this.sourceName = sourceName;
        this.landingPage = landingPage;
        this.type = type;
        this.width = width;
        this.height = height;
        this.campaignId = campaignId;
        this.creativeType = creativeType;
        this.displayType = displayType;
        this.flightId = flightId;
        this.buyType = buyType;
        this.bookingType = bookingType;
        this.frequency = frequency;
        this.weight = weight;
        this.totalBookings = totalBookings;
        this.dailyBookings = dailyBookings;
        this.dateRanges = dateRanges;
        this.publishers = publishers;
        this.macAddress = macAddress;
    }

    public int getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(int creativeId) {
        this.creativeId = creativeId;
    }

    public String getCreativeName() {
        return creativeName;
    }

    public void setCreativeName(String creativeName) {
        this.creativeName = creativeName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getCreativeType() {
        return creativeType;
    }

    public void setCreativeType(String creativeType) {
        this.creativeType = creativeType;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getBuyType() {
        return buyType;
    }

    public void setBuyType(String buyType) {
        this.buyType = buyType;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public List<DateRange> getDateRanges() {
        return dateRanges;
    }

    public void setDateRanges(List<DateRange> dateRanges) {
        this.dateRanges = dateRanges;
    }

    public List<PublisherData> getPublishers() {
        return publishers;
    }

    public void setPublishers(List<PublisherData> publishers) {
        this.publishers = publishers;
    }

    public List<String> getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(List<String> macAddress) {
        this.macAddress = macAddress;
    }

    public int getTotalBookings() {
        return totalBookings;
    }

    public void setTotalBookings(int totalBookings) {
        this.totalBookings = totalBookings;
    }

    public int getDailyBookings() {
        return dailyBookings;
    }

    public void setDailyBookings(int dailyBookings) {
        this.dailyBookings = dailyBookings;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
