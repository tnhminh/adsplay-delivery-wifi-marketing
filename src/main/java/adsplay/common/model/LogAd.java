package adsplay.common.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogAd {

    @Expose
    @SerializedName("type")
    private String type = "";

    @Expose
    @SerializedName("impression")
    private List<String> impressionTracking = new ArrayList<String>();

    @Expose
    @SerializedName("click")
    private List<String> clickTracking = new ArrayList<String>();

    public LogAd(String type, List<String> impressionTracking, List<String> clickTracking) {
        super();
        this.type = type;
        this.impressionTracking = impressionTracking;
        this.clickTracking = clickTracking;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getImpressionTracking() {
        return impressionTracking;
    }

    public void setImpressionTracking(List<String> impressionTracking) {
        this.impressionTracking = impressionTracking;
    }

    public List<String> getClickTracking() {
        return clickTracking;
    }

    public void setClickTracking(List<String> clickTracking) {
        this.clickTracking = clickTracking;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(this);
    }

}
