package adsplay.common.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WifiBannerModel {

    @Expose
    @SerializedName("placement_id")
    private int placementId = 0;

    @Expose
    @SerializedName("creative_id")
    private int creativeId = 0;

    @Expose
    @SerializedName("width")
    private String width = "";

    @Expose
    @SerializedName("height")
    private String height = "";

    @Expose
    @SerializedName("display_type")
    private String displayType = "";

    @Expose
    @SerializedName("creative_type")
    private String creativeType = "";

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("landing_page")
    private String landingPage = "";

    @Expose
    @SerializedName("sources")
    private List<String> sources = new ArrayList<String>();

    @Expose
    @SerializedName("log_ads")
    List<LogAd> logAds = new ArrayList<LogAd>();

    public WifiBannerModel(int placementId, int creativeId, String width, String height, String displayType, String creativeType, String name,
            String landingPage, List<String> sources, List<LogAd> logAds) {
        super();
        this.placementId = placementId;
        this.creativeId = creativeId;
        this.width = width;
        this.height = height;
        this.displayType = displayType;
        this.creativeType = creativeType;
        this.name = name;
        this.landingPage = landingPage;
        this.sources = sources;
        this.logAds = logAds;
    }

    public int getPlacementId() {
        return placementId;
    }

    public void setPlacementId(int placementId) {
        this.placementId = placementId;
    }

    public int getCreativeId() {
        return creativeId;
    }

    public void setCreativeId(int creativeId) {
        this.creativeId = creativeId;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public List<LogAd> getLogAds() {
        return logAds;
    }

    public void setLogAds(List<LogAd> logAds) {
        this.logAds = logAds;
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public String getCreativeType() {
        return creativeType;
    }

    public void setCreativeType(String creativeType) {
        this.creativeType = creativeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(this);
    }
}
