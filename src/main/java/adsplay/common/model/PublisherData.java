package adsplay.common.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PublisherData {

    @Expose
    @SerializedName("publisher_id")
    private int publisherId;

    @Expose
    @SerializedName("timezone")
    private String timeZone;

    @Expose
    @SerializedName("placement_ids")
    private List<Integer> placementIds = new ArrayList<Integer>();

    public PublisherData(int publisherId, String timeZone, List<Integer> placementIds) {
        super();
        this.publisherId = publisherId;
        this.timeZone = timeZone;
        this.placementIds = placementIds;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public List<Integer> getPlacementIds() {
        return placementIds;
    }

    public void setPlacementIds(List<Integer> placementIds) {
        this.placementIds = placementIds;
    }

}
