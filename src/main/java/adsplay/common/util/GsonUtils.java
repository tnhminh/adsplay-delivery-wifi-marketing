package adsplay.common.util;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

public class GsonUtils {

    private static Gson gson = new Gson();

    private static JsonParser jsonParser = new JsonParser();

    public static Object convertJsonToObject(String json, Class<?> type) {
        return gson.fromJson(json, type);
    }
}
