package adsplay.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.adsplay.sentry.handler.SentryHanlder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import adsplay.delivery.redis.cache.model.Creative;
import rfx.core.util.LogUtil;

public class ParserUtils {

    private static final String COLON = ":";
    private static final String ANY_SPACE = "\\s+";
    private static final String COMMA = ",";
    private static Gson gson = new Gson();

    // {a=x, b=y}
    public static void parseAndUpdateStringToMap(String mapString, Map<String, String> maps, String keyName) {
        if (mapString == null) {
            return;
        }
        String value = "";
        value = mapString.substring(1, mapString.length() - 1); // remove curly
        // brackets
        String[] keyValuePairs = value.split(","); // split the string to creat
                                                   // key-value pairs
        Map<String, String> childMap = new HashMap<String, String>();
        for (String pair : keyValuePairs) // iterate over the pairs
        {
            String replacedPair = pair.replaceAll(ANY_SPACE, "").replace("\"", "");
            String[] entry = replacedPair.split("="); // split the pairs to get
                                                      // key and
            // value
            if (entry.length == 1) {
                entry = replacedPair.startsWith("value") ? replacedPair.split(COLON, 2) : replacedPair.split(COLON);
            }
            childMap.put(entry[0].trim(), entry[1].trim()); // add them to the
            // hashmap and trim
            // whitespaces
        }
        if (!childMap.isEmpty()) {
            maps.put(keyName, converMapToJSON(childMap));
        }
    }

    // "[a, b]"
    public static void parseAndAddStringToArray(String stringArr, Map<String, String> maps, String keyName) {
        if (stringArr == null) {
            return;
        }
        StringBuilder strBuilder = new StringBuilder();
        String[] arr = convertStringToArray(stringArr);
        strBuilder.append("[");
        int i = 0;
        String separator = "";
        while (i < arr.length) {
            strBuilder.append(separator);
            String val = (arr[i].indexOf("\"") != -1 ? arr[i].replace("\"", "") : arr[i]).replace("'", "").trim();
            strBuilder.append("\"").append(val).append("\"");
            separator = ",";
            i++;
        }
        strBuilder.append("]");
        maps.put(keyName, strBuilder.toString());
    }

    public static String[] convertStringToArray(String stringArr) {
        return convertStringToArray(stringArr, null);
    }
    
    public static String[] convertStringToArray(String stringArr, String [] defaultValues) {
        String[] arr = null;
        if (!stringArr.isEmpty()) {
            arr = stringArr.replace("[", "").replaceAll("]", "").replaceAll("\\s+", "").replace("'", "").replace("\"", "").split(",");
            return arr;
        }
        if (defaultValues == null){
            return new String[0];
        }
        return defaultValues;
    }

    public static String standalizeRedisMapToJSON(Map<String, String> maps) {
        List<String> removedField = new ArrayList<String>();
        if (maps == null) {
            return "";
        }
        maps.forEach((key, value) -> {
            try {
                String strVal = value.trim();
                if ("[]".equals(strVal) || "".equals(strVal)) {
                    removedField.add(key);
                    SentryHanlder.sendErrorToSentryLog("injectToPersistent", "minhtnh2@fpt.com.vn", null, null, "", new Exception("Field with missing value was found !!!" + ", redisMapping:" + maps), false);
                } else if (strVal.startsWith("{") || strVal.endsWith("}")) {
                    parseAndUpdateStringToMap(strVal, maps, key);
                } else if (strVal.startsWith("[{") || strVal.endsWith("]}")) {
                    strVal = strVal.replace("'", "\"");
                    maps.put(key, strVal);
                    // Nested object in array
                } else if ("third_trackings".equals(key)){
                    strVal = strVal.replace("\"[", "[").replace("]\"", "]").replace("\\", "");
                    maps.put(key, strVal);
                } else if (strVal.startsWith("[") || strVal.endsWith("]")) {
                    parseAndAddStringToArray(strVal, maps, key);
                }
                
                if (strVal.contains("[timestamp]")) {
                    strVal = strVal.replace("[timestamp]", "'[timestamp]'");
                    maps.put(key, strVal);
                } 
            } catch (Exception e) {
                LogUtil.error(e);
            }
        });
        
        // Remove invalid field to avoid exception 
        if (!removedField.isEmpty()){
            removedField.forEach(removedKey -> {
                maps.remove(removedKey);
            });
        }
        return converMapToJSON(maps);
    }
    
    public static Object injectDataToPersistence(Map<String, String> maps, Class<?> klass) {
        return gson.fromJson(standalizeRedisMapToJSON(maps), klass);
    }

    public static String converMapToJSON(Map<String, ? extends Object> maps) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(maps).replace("\"{", "{").replace("}\"", "}").replace("\"[", "[").replace("]\"", "]").replace("\\", "");
    }
    
    public static String convertListToStringArray(List<String> fwIds) {
        StringBuilder wVals = new StringBuilder();
        wVals.append("[");
        String[] separatorVal = new String[1];
        separatorVal[0] = "";
        fwIds.forEach(flightId -> {
            wVals.append(separatorVal[0]);
            wVals.append(flightId);
            separatorVal[0] = COMMA;
        });
        wVals.append("]");
        return wVals.toString();
    }
    
    public static long ipToLong(String ipAddress) {
        
        long result = 0;
                
        String[] ipAddressInArray = ipAddress.split("\\.");

        for (int i = 3; i >= 0; i--) {
                        
                long ip = Long.parseLong(ipAddressInArray[3 - i]);
                        
                //left shifting 24,16,8,0 and bitwise OR
                        
                //1. 192 << 24
                //1. 168 << 16
                //1. 1   << 8
                //1. 2   << 0
                result |= ip << (i * 8);
                
        }

        return result;
  }

    public static void main(String[] args) {
        Gson gson = new Gson();
        // Map<String, String> map = new HashMap<String, String>();
        // // parsingStringToArray("[a, b, c, d]", true, new HashMap<String,
        // // Object>(), "keyArr");
        // parseAndUpdateStringToMap("{a=x, b=y}", map, "newKey");
        // System.out.println(converMapToJSON(map));
        Creative crt = (Creative) ParserUtils.injectDataToPersistence(RedisCache.hget("creative_delivery_59", RedisPool.CREATIVE), Creative.class);
        System.out.println(crt);
        //System.out.println(standalizeRedisMapToJSON(RedisCache.hget("creative_delivery_48", RedisPool.CREATIVE)));
    }
}
