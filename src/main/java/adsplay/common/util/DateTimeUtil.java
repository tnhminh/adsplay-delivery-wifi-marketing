package adsplay.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import rfx.core.util.LogUtil;

public class DateTimeUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final String YYYY_MM_DD_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";

    public static final String HH_MM_SS = "HH:mm:ss";

    public static final String YYYYMMDD = "yyyyMMdd";

    public static final String YYYYMMDDHH = "yyyyMMddHH";

    private static SimpleDateFormat dateFormat = new SimpleDateFormat(YYYY_MM_DD);

    public static String convertDateToStringFormat(Date date, String timeZone, String pattern) {
        if (!pattern.isEmpty()) {
            SimpleDateFormat localSdF = new SimpleDateFormat(pattern);
            localSdF.setTimeZone(TimeZone.getTimeZone(timeZone));
            return localSdF.format(date);
        }
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        return dateFormat.format(date);
    }

    public static Calendar getCurrentDateTimeZone(String timeZone, boolean isNow, Integer yyyy, Integer month, Integer date, Integer hour,
            Integer minute, Integer second, Integer milisecs) {
        TimeZone tz = TimeZone.getTimeZone(timeZone);
        Calendar calendar = Calendar.getInstance(tz);
        if (!isNow) {
            calendar.set(Calendar.YEAR, yyyy);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, date);
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, second);
            calendar.set(Calendar.MILLISECOND, milisecs);
        }
        return calendar;
    }

    public static Calendar getCurrentDateTimeZone(String timeZone, boolean isNow) {
        return getCurrentDateTimeZone(timeZone, isNow, 0, 0, 0, 0, 0, 0, 0);
    }

    public static String getCurrentHour(String timeZone, boolean isNow) {
        return String.valueOf(getCurrentDateTimeZone(timeZone, isNow).get(Calendar.HOUR_OF_DAY));
    }

    public static String getCurrentDateOfWeek(String timeZone, boolean isNow) {
        return String.valueOf(getCurrentDateTimeZone(timeZone, isNow).get(Calendar.DAY_OF_WEEK) - 1);
    }

    public static Date getCurrentDateTimeZone(String timeZone, Integer year, Integer month, Integer date, Integer hour, Integer minute,
            Integer second, Integer milisecs) {
        return getCurrentDateTimeZone(timeZone, false, year, month, date, hour, minute, second, milisecs).getTime();
    }

    public static String getCurrentDateString(String timeZone, String pattern, boolean isNow, Integer year, Integer month, Integer date) {
        return convertDateToStringFormat(getCurrentDateTimeZone(timeZone, isNow, year, month, date, 0, 0, 0, 0).getTime(), timeZone,
                pattern);
    }

    public static String getCurrentDateString(String timeZone, String pattern, boolean isNow, Integer year, Integer month, Integer date,
            Integer hour, Integer minute, Integer second, Integer milisecs) {
        return convertDateToStringFormat(
                getCurrentDateTimeZone(timeZone, isNow, year, month, date, hour, minute, second, milisecs).getTime(), timeZone, pattern);
    }

    public static String getDateFromSeconds(long totalSecs) {
        return String.format("%02d:%02d:%02d", totalSecs / 3600, (totalSecs % 3600) / 60, totalSecs % 60);
    }

    public static long getTimeFromStringFormat(String strTime, String pattern) throws ParseException {
        long seconds = 0L;
        try {
            DateFormat dateFormat = new SimpleDateFormat(pattern);
            Date reference = dateFormat.parse("00:00:00");
            Date date = dateFormat.parse(strTime);
            return (date.getTime() - reference.getTime()) / 1000L;
        } catch (Exception e) {
            LogUtil.error(DateTimeUtil.class, "getTimeFromStringFormat()", "Error when parsing time from string format !!!");
            LogUtil.error(e);
        }
        return seconds;

    }

    public static int currentUnixTimestamp() {
        return (int) (System.currentTimeMillis() / 1000L);
    }

}
