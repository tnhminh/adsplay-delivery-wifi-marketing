package adsplay.common;

public class SpotXCommonTag {

  public static final String SPOTX_PREROLL_WEB = "192458";
  public static final String SPOTX_MIDROLL_1_WEB = "204838";
  public static final String SPOTX_MIDROLL_2_WEB = "204839";
  public static final String SPOTX_POSROLL_WEB = "204840";

  public static final String SPOTX_PREROLL_APP = "198475";
  public static final String SPOTX_MIDROLL_1_APP = "205634";
  public static final String SPOTX_MIDROLL_2_APP = "205636";
  public static final String SPOTX_POSROLL_APP = "205638";

  public static final String SPOTX_WEB_TAG =
      "https://search.spotxchange.com/vast/2.0/[placementSpotX]?VPAID=JS&content_page_url=[content_page_url]&cb=[timestamp]&player_width=360&player_height=640";
  public static final String SPOTX_APP_TAG =
      "https://search.spotxchange.com/vast/2.0/[placementSpotX]?VPI=MP4&app[bundle]=vn.danet.fptplay&device[ifa]=MB&player_height=1080&player_width=1920&device[devicetype]=3&device[make]=FPT&device[model]=MB&app[name]=FPTPlay";
}