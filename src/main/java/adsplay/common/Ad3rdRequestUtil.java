package adsplay.common;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import rfx.core.util.LogUtil;
import rfx.core.util.StringPool;

public class Ad3rdRequestUtil {
	final static int MAX_SIZE = 20;
	public static final Charset CHARSET_UTF8 = Charset.forName(StringPool.UTF_8);
	public static final String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36";
	public static final String MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; Android 5.0; SAMSUNG-SM-G925A Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36";
	
	
	public static String executeGet(final String url){
		try {
			return executeGet(new URL(url), USER_AGENT);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "";
	}
	
	public static String executeGet(final String url, final String userAgent){
		try {
			return executeGet(new URL(url), userAgent);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return "";
	}
	
	
	static ConcurrentMap<Integer, HttpClient> httpClientPool = new ConcurrentHashMap<>(MAX_SIZE);
	
	public static final HttpClient getThreadSafeClient() throws Exception {
		int slot = (int)(Math.random() * (MAX_SIZE + 1));		
	    return getThreadSafeClient(slot);
	}
	
	public static final HttpClient getThreadSafeClient(int slot) throws Exception {		
		HttpClient httpClient = httpClientPool.get(slot);
		if(httpClient == null){
			PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();			
			cm.setMaxTotal(20);			
			cm.setDefaultMaxPerRoute(20);		
			HttpHost localhost = new HttpHost("locahost", 80);
			cm.setMaxPerRoute(new HttpRoute(localhost), 20);
			httpClient = HttpClients.custom().setConnectionManager(cm).build();
		    httpClientPool.put(slot, httpClient);
		}
	    return httpClient;
	}
	
	public static String executeGet(final URL url, final String userAgent){
		HttpResponse response = null;
		HttpClient httpClient = null;
	
		try {
			HttpGet httpget = new HttpGet(url.toURI());		
			
			httpget.setHeader("User-Agent", userAgent );
			httpget.setHeader("Accept-Charset", "utf-8");
			httpget.setHeader("Accept", "text/html,application/xhtml+xml");
			httpget.setHeader("Cache-Control", "max-age=3, must-revalidate, private");	
			httpget.setHeader("Host", url.getHost());
			httpget.setHeader("Origin", "https://fptplay.net");
			httpget.setHeader("Referer", "https://fptplay.net");			
			
			if(url.toString().startsWith("https")){
				httpget.setHeader("Upgrade-Insecure-Requests", "1");	
			}
	
			httpClient  = getThreadSafeClient();
			
			response = httpClient.execute(httpget);	
			
			int code = response.getStatusLine().getStatusCode();
			if (code == 200) {				
				HttpEntity entity = response.getEntity();
				if (entity != null) {										
					String html = EntityUtils.toString(entity, CHARSET_UTF8);
					return html;
				}
			} else if(code == 404) {
				return "404";
			} else {
				return "500";
			}
        } catch (Throwable e) {
            try {
                LogUtil.error(Ad3rdRequestUtil.class, "executeGet", "Error when GET vast tag:" + url.toURI());
            } catch (URISyntaxException e1) {
                e1.printStackTrace();
            }
            LogUtil.error(e);
            return "444";
        } finally {
			response = null;
		}
		return "";
	}
}

