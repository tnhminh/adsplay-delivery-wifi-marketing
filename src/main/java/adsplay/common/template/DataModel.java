package adsplay.common.template;

import java.util.List;

import io.netty.handler.codec.http.HttpHeaders;



/**
 * the base model for HttpProcessor implementations
 * 
 * @author trieu
 *
 */
public interface DataModel {
  /**
   * free closable resources, better for JVM GC
   */
  void freeResource();

  /**
   * @return String the class-path of implemented class
   */
  String getClasspath();

  /**
   * @return true if the model can be processed and rendered to text
   */
  boolean isOutputable();

  static String getClasspath(DataModel e) {
    String classpath = e.getClass().getName();
    // System.out.println("---buildClasspath "+classpath);
    return classpath;
  }

  static String getClasspath(Class<?> e) {
    return e.getName();
  }

  List<HttpHeaders> getHttpHeaders();
}
