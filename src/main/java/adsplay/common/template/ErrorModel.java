package adsplay.common.template;

import java.util.List;

import org.apache.commons.codec.binary.Base64;

import io.netty.handler.codec.http.HttpHeaders;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.RandomUtil;

public class ErrorModel implements DataModel {

    private String adId = "";

    private String url = "";

    private String domain = "";

    public ErrorModel(String domain, String url, boolean isSSL, int placementId) {
        super();
        this.url = url + "&placement=" + placementId + "&serverTime=" + DateTimeUtil.currentUnixTimestamp();
        this.domain = (isSSL ? "https://" : "http://") + domain;
        this.adId = Base64.encodeBase64URLSafeString(String.valueOf(RandomUtil.getRandom(10000) + placementId).getBytes());
    }

    public String getAdId() {
        return adId;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void freeResource() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getClasspath() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isOutputable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public List<HttpHeaders> getHttpHeaders() {
        // TODO Auto-generated method stub
        return null;
    }

}
