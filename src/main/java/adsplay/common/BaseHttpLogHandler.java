package adsplay.common;

import static io.vertx.core.http.HttpHeaders.CONNECTION;
import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static io.vertx.core.http.HttpHeaders.SET_COOKIE;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import adsplay.common.LocationUtil.LocationCacheObj;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;
import server.http.handler.BaseHttpHandler;
import server.http.util.HttpTrackingUtil;

public abstract class BaseHttpLogHandler implements BaseHttpHandler {

  public final static String DEFAULT_DOMAIN = ".adsplay.net";
  public final static String DEFAULT_PATH = "/";
  public final static String FOSP_AID_NAME = "fosp_aid";
  public final static String FOSP_GENDER_NAME = "fosp_gender";
  public final static String adsplay_SESSION_NAME = "adsplay_session";
  public final static String FOSP_TRACKING_VERSION = "eatv";
  public final static String FOSP_COUNTRY_CODE = "fosp_country";
  public final static String FOSP_PROVINCE_CODE = "fosp_loc_province";
  public final static String FOSP_LOCATION_NAME = "fosp_location";
  public final static String FOSP_LOCATION_ZONE_NAME = "fosp_location_zone";

  private static final String _0_0_0_0 = "0.0.0.0";
  private static final String API = "/api";
  private static final String CROSSDOMAIN_XML = "/crossdomain.xml";
  private static final String unknown = "unknown";
  public final static int COOKIE_AGE_1_WEEK = 604800; // One week
  public final static int COOKIE_AGE_10_YEAR = 31557600 * 10; // 10 years

  static String getCrossdomainFile() {
    StringBuilder s = new StringBuilder();
    s.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    s.append(
        "<!DOCTYPE cross-domain-policy SYSTEM \"http://www.adobe.com/xml/dtds/cross-domain-policy.dtd\">");
    s.append("<cross-domain-policy>");
    s.append("<site-control permitted-cross-domain-policies=\"all\" />");
    s.append("<allow-access-from domain=\"*\" secure=\"false\"/>");
    s.append("<allow-http-request-headers-from	domain=\"*\" headers=\"*\" secure=\"false\" />");
    s.append("</cross-domain-policy>");
    return s.toString();
  }

  final String crossdomainFile = getCrossdomainFile();
  protected static final String PONG = "PONG";
  protected static final String FAVICON_ICO = "/favicon.ico";
  protected static final String PING = "/ping";

  protected static final String getKafkaKey(String logUri) {
    return logUri.replaceAll("/", "_");
  }

  public BaseHttpLogHandler() {
    super();
  }

  protected final void handleCrossdomain(final HttpServerRequest req) {
    Buffer buffer = Buffer.buffer(crossdomainFile);
    HttpServerResponse response = req.response();
    MultiMap headers = response.headers();
    headers.set(CONTENT_TYPE, "application/xml");
    headers.set(CONTENT_LENGTH, String.valueOf(buffer.length()));
    headers.set(CONNECTION, HttpTrackingUtil.HEADER_CONNECTION_CLOSE);
    req.response().end(buffer);
  }

  @Override
  public String getPathKey() {
    return StringPool.STAR;
  }


  public static String getRemoteIP(HttpServerRequest request) {
    try {
      io.vertx.core.net.SocketAddress remoteAddress = request.remoteAddress();
      if (remoteAddress instanceof InetSocketAddress) {
        return ((InetSocketAddress) remoteAddress).getAddress().getHostAddress();
      }
      return remoteAddress.toString().split("/")[1].split(":")[0];
    } catch (Throwable e) {
      e.printStackTrace();
    }
    return _0_0_0_0;
  }

  public static String getRequestIP(HttpServerRequest request) {
    String ipAddress = request.headers().get("X-Forwarded-For");
    if (!StringUtil.isNullOrEmpty(ipAddress) && !unknown.equalsIgnoreCase(ipAddress)) {
      String[] toks = ipAddress.split(",");
      int len = toks.length;
      if (len > 1) {
        ipAddress = toks[len - 1];
      } else {
        return ipAddress;
      }
    } else {
      ipAddress = getRemoteIP(request);
    }
    return ipAddress;
  }

  public static String location2JSON(String ip, LocationCacheObj locObj) {
    Map<String, Map<String, Object>> map = new HashMap<>(1);
    Map<String, Object> locMap = new HashMap<>(4);
    locMap.put("ip", ip);
    locMap.put("country", locObj.getCountryCode());
    locMap.put("zone", locObj.getZone());
    locMap.put("province", locObj.getProvince());
    map.put("loc", locMap);
    return new Gson().toJson(map);
  }

  public static Cookie createCookie(String name, String value, String domain, String path) {
    Cookie cookie = new DefaultCookie(name, value);
    cookie.setDomain(domain);
    cookie.setPath(path);
    return cookie;
  }

  protected boolean handleCommonRequest(String uri, HttpServerRequest req,
      HttpServerResponse resp) {

    // data service
    if (uri.startsWith(API)) {
      String name = StringUtil.safeString(req.params().get("name"));
      if ("ip2location".equals(name)) {
        String remoteIP = getRequestIP(req);
        String ip = StringUtil.safeString(req.params().get("ip"), remoteIP);
        String callback = StringUtil.safeString(req.params().get("callback"));
        LocationCacheObj locObj = Ip2LocationUtil.find(ip);

        MultiMap headers = resp.headers();

        Cookie fosp_location_cookie = createCookie(FOSP_LOCATION_NAME,
            String.valueOf(locObj.getProvince()), DEFAULT_DOMAIN, DEFAULT_PATH);
        fosp_location_cookie.setMaxAge(COOKIE_AGE_1_WEEK);
        headers.add(SET_COOKIE, ServerCookieEncoder.LAX.encode(fosp_location_cookie));

        Cookie fosp_country = createCookie(FOSP_COUNTRY_CODE,
            String.valueOf(locObj.getCountryCode()), DEFAULT_DOMAIN, DEFAULT_PATH);
        fosp_country.setMaxAge(COOKIE_AGE_1_WEEK);
        headers.add(SET_COOKIE, ServerCookieEncoder.LAX.encode(fosp_country));

        Cookie fosp_location_zone_cookie = createCookie(FOSP_LOCATION_ZONE_NAME,
            String.valueOf(locObj.getZone()), DEFAULT_DOMAIN, DEFAULT_PATH);
        fosp_location_zone_cookie.setMaxAge(COOKIE_AGE_1_WEEK);
        headers.add(SET_COOKIE, ServerCookieEncoder.LAX.encode(fosp_location_zone_cookie));

        String json = location2JSON(ip, locObj);
        if (StringUtil.isEmpty(callback)) {
          resp.putHeader(CONTENT_TYPE, StringPool.MediaType.JSON);
          resp.end(json);
        } else {
          resp.putHeader(CONTENT_TYPE, StringPool.MediaType.JAVASCRIPT);
          StringBuilder s = new StringBuilder(";");
          s.append(callback).append("(").append(json).append(");");
          resp.end(s.toString());
        }
        RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), "ip2loc");
        return true;
      } else {
        resp.end("Not handler found for service:" + name);
        return true;
      }
    }

    // default for all cases
    else if (uri.equalsIgnoreCase(FAVICON_ICO)) {
      HttpTrackingUtil.trackingResponse(req);
      return true;
    } else if (uri.equalsIgnoreCase(PING)) {
      resp.end(PONG);
      return true;
    } else if (uri.equalsIgnoreCase(CROSSDOMAIN_XML)) {
      handleCrossdomain(req);
      return true;
    }
    return false;
  }

    public static void setCorsHeaders(MultiMap headers, List<String> pmIds, String origin) {
        headers.set("Accept-Ranges", "bytes");
        if (pmIds.contains("101") || pmIds.contains("102") || pmIds.contains("333")) {
            headers.set("Access-Control-Allow-Origin", origin);
        } else {
            headers.set("Access-Control-Allow-Origin", "*");
        }
        // headers.set("Access-Control-Allow-Origin", origin);
        headers.set("Access-Control-Allow-Credentials", "true");
        headers.set("Access-Control-Allow-Methods", "GET,POST");
        headers.set("Access-Control-Allow-Headers", "Content-Type, Range, *");
        headers.set("Access-Control-Expose-Headers", "Content-Range");
        headers.set("Access-Control-Max-Age", "86400");
        headers.set("Cache-Control", "no-cache,no-store");
        headers.set("Connection", "Keep-Alive");
    } 

}
