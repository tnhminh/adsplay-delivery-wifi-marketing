package adsplay.delivery.function;

public interface BookingFilter {

    boolean isAvailableBooking();

}
