package adsplay.delivery.function;

import adsplay.delivery.properties.MessageProperties;

public interface Targeting {

    public static final String EMPTY_VAL = MessageProperties.getProperty("general.zero", "");

    public static final String TOTAL = MessageProperties.getProperty("booking.total.field", "");

    public static final String DAILY = MessageProperties.getProperty("booking.daily.field", "");

    public static final String BOOKING = MessageProperties.getProperty("booking.delivery.prefix", "");
    
    public static final String BOOKING_TYPE = MessageProperties.getProperty("booking.type.field", "");

    public static final String UNDERLINE = MessageProperties.getProperty("general.dash", "");

    public static final String BK = MessageProperties.getProperty("general.bk", "");

    public static final String PLACEMENT_PREFIX = MessageProperties.getProperty("placement.delivery.prefix", "");

    public static final String CATEGORY_PREFIX = MessageProperties.getProperty("category.delivery.prefix", "");

    public static final String PROVINCES_PREFIX = MessageProperties.getProperty("provinces.delivery.prefix", "");
    
    public static final String UTC = MessageProperties.getProperty("general.utc", "");

    boolean isTargetingOn(String flightParam, Object rqParam);
}
