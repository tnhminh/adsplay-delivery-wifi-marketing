package adsplay.delivery.function;

import java.util.List;

public interface CategoryFilter {

    boolean isContainCategory(List<String> categories);

}
