package adsplay.delivery.function;

import java.util.List;
import java.util.Map;

import adsplay.delivery.properties.MessageProperties;
import io.vertx.core.MultiMap;

public interface Mapping {

    public static final String GEO = MessageProperties.getProperty("api.geo", "");
    
    public static final String DATEHOUR = MessageProperties.getProperty("general.datehour", "");
    
    public static final String AD_VIEW = MessageProperties.getProperty("placement.ad_view.field", "");
    
    public static final String REQUEST = MessageProperties.getProperty("general.request", "");
    
    public static final String DASH = MessageProperties.getProperty("general.dash", "");
    
    public static final String DATE = MessageProperties.getProperty("general.date", "");
    
    public static final String INV = MessageProperties.getProperty("general.inv", "");
    
    public static final String UTC = MessageProperties.getProperty("general.utc", "");
    
    public static final String PLACEMENT_PREFIX = MessageProperties.getProperty("placement.delivery.prefix", "");
    
    public static final String ZERO = MessageProperties.getProperty("general.zero", "");
    
    public static final String CONTENT_TO_CATEGORIES = MessageProperties.getProperty("api.ctc.prefix", "");
    
    public static final String PROVINCES_PREFIX = MessageProperties.getProperty("provinces.delivery.prefix", "");
    
    public static final String CATEGORY_PREFIX = MessageProperties.getProperty("category.delivery.prefix", "");

    void fillParamsMapping(MultiMap params, List<String> pmIds, Map<String, Object> paramsAsMap, Map<String, Object> additionalInfos);

    void fillParamsMapping(MultiMap params, String remoteIpAddr, List<String> pmIds, Map<String, Object> paramsAsMap,
            Map<String, Object> additionalInfos);
}
