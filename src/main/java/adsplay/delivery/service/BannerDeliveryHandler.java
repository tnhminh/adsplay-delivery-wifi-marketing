package adsplay.delivery.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adsplay.common.model.CreativeData;
import adsplay.common.util.GsonUtils;
import adsplay.delivery.build.result.BuildingResultService;
import adsplay.delivery.field.metadata.MetaData;
import adsplay.delivery.filter.FilterService;
import adsplay.delivery.filter.WeightFilterService;
import adsplay.delivery.job.AbstractDeliveryRequestJob;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class BannerDeliveryHandler extends AbstractDeliveryHandler {

    private WeightFilterService weightFilterService;

    public BannerDeliveryHandler(FilterService filterService, MetaData filedMetaData, BuildingResultService buildingResultService,
            WeightFilterService weightFilterService) {
        super(filterService, filedMetaData, buildingResultService);
        this.weightFilterService = weightFilterService;
    }

    @Override
    public String executeJob(AbstractDeliveryRequestJob job) {

        CreativeData finalCrt = null;
        try {
            String rawData = StringUtil.safeString(RedisCache.get("creative_wifi_all", RedisPool.CREATIVE), "");

            CreativeData[] cacheCreative = (CreativeData[]) GsonUtils.convertJsonToObject(rawData, CreativeData[].class);

            List<CreativeData> crtDatas = new ArrayList<CreativeData>(Arrays.asList(cacheCreative));

            Map<String, Object> paramAsMap = job.getParamAsMap();
            int placementId = job.getPlacementId();

            Map<Integer, Integer> crtIdWeightPairs = new HashMap<Integer, Integer>();

            List<CreativeData> selectedCrts = filterService.doFilterAndTargeting(fieldMetaData.getTargetingEnums(), crtDatas, placementId,
                    paramAsMap, crtIdWeightPairs);

            if (selectedCrts.isEmpty()) {
                return "";
            }

            finalCrt = weightFilterService.doFilter(selectedCrts);

            return buildingResultService.buildingResult(finalCrt, paramAsMap, placementId);
        } catch (Exception e) {
            LogUtil.error(BannerDeliveryHandler.class, "execute()",
                    "Error when execute job data !!!" + StringUtil.safeParseInt(finalCrt.getCreativeId()));
            LogUtil.error(e);
        }
        return "";

    }

}
