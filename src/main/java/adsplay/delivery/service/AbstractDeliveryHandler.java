package adsplay.delivery.service;

import adsplay.delivery.build.result.BuildingResultService;
import adsplay.delivery.field.metadata.MetaData;
import adsplay.delivery.filter.FilterService;
import adsplay.delivery.job.ExecuteJobService;

public abstract class AbstractDeliveryHandler implements ExecuteJobService {

    protected FilterService filterService;

    protected MetaData fieldMetaData;

    protected BuildingResultService buildingResultService;

    public AbstractDeliveryHandler(FilterService filterService, MetaData filedMetaData, BuildingResultService buildingResultService) {
        super();
        this.filterService = filterService;
        this.fieldMetaData = filedMetaData;
        this.buildingResultService = buildingResultService;
    }

    public FilterService getFilterService() {
        return filterService;
    }

    public void setFilterService(FilterService filterService) {
        this.filterService = filterService;
    }

    public MetaData getFiledMetaData() {
        return fieldMetaData;
    }

    public void setFiledMetaData(MetaData filedMetaData) {
        this.fieldMetaData = filedMetaData;
    }

    public BuildingResultService getBuildingResultService() {
        return buildingResultService;
    }

    public void setBuildingResultService(BuildingResultService buildingResultService) {
        this.buildingResultService = buildingResultService;
    }
}
