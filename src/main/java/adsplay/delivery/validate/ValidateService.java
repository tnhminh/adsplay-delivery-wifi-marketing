package adsplay.delivery.validate;

public interface ValidateService {
    String validateJsonData(String jsonData);
}
