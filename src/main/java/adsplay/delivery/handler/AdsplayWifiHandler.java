package adsplay.delivery.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adsplay.delivery.context.SpringContextUtil;
import adsplay.delivery.function.Mapping;
import adsplay.delivery.job.AbstractDeliveryRequestJob;
import adsplay.delivery.job.WifiDeliveryJob;
import adsplay.delivery.redis.cache.mapping.WifiParamMapping;
import adsplay.delivery.service.AbstractDeliveryHandler;
import io.vertx.core.MultiMap;
import rfx.core.util.StringUtil;

public class AdsplayWifiHandler {

    private static AbstractDeliveryHandler bannerDeliveryHandler = (AbstractDeliveryHandler) SpringContextUtil.getContext()
            .getBean("bannerDeliveryHandler");

    private static Mapping mapping = new WifiParamMapping();

    public static String handle(MultiMap params, String remoteIP, List<String> pmIds, Map<String, Object> additionalInfos) {

        // ========GET ALL PARAMS FPT-PLAY===================
        Map<String, Object> paramsAsMap = new HashMap<String, Object>();

        mapping.fillParamsMapping(params, remoteIP, pmIds, paramsAsMap, additionalInfos);

        AbstractDeliveryRequestJob job = new WifiDeliveryJob("wifi-delivery", "wifi-marketing", paramsAsMap,
                StringUtil.safeParseInt(pmIds.get(0)));

        return bannerDeliveryHandler.executeJob(job);
    }
}