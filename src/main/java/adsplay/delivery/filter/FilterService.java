package adsplay.delivery.filter;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import adsplay.common.model.CreativeData;
import adsplay.common.model.DateRange;
import adsplay.delivery.field.metadata.TargetEnum;
import adsplay.delivery.properties.MessageProperties;

public interface FilterService {

    public static final String EMPTY_VAL = MessageProperties.getProperty("general.zero", "");

    public static final String TOTAL = MessageProperties.getProperty("booking.total.field", "");

    public static final String DAILY = MessageProperties.getProperty("booking.daily.field", "");

    public static final String BOOKING = MessageProperties.getProperty("booking.delivery.prefix", "");

    public static final String BOOKING_TYPE = MessageProperties.getProperty("booking.type.field", "");

    public static final String UNDERLINE = MessageProperties.getProperty("general.dash", "");

    public static final String BK = MessageProperties.getProperty("general.bk", "");

    public static final String PLACEMENT_PREFIX = MessageProperties.getProperty("placement.delivery.prefix", "");

    public static final String CATEGORY_PREFIX = MessageProperties.getProperty("category.delivery.prefix", "");

    public static final String PROVINCES_PREFIX = MessageProperties.getProperty("provinces.delivery.prefix", "");

    public static final String UTC = MessageProperties.getProperty("general.utc", "");

    List<CreativeData> doFilterAndTargeting(List<TargetEnum> targetMetaDatas, List<CreativeData> creativeDatas, int placementId, Map<String, Object> params, Map<Integer, Integer> crtIdWeightPairs);

    boolean isIncludeDateOfWeek(List<Integer> dateOfWeeks, String timeZone);

    boolean isIncludeHour(List<Integer> hours, String timeZone);

    boolean isIncludeDateRange(String fromDate, String toDate, String timeZone);

    boolean isAvailableBooking(String flightId, int totalBooking, int dailyBooking, String timeZone);
    
    boolean isTargetingOnMacAddress(List<String> macAddressList, String macParam, List<TargetEnum> targetedMetaDatas);
    
    boolean doTargetingOnTime(Iterator<DateRange> dateRangeIte, String timezone, List<TargetEnum> targetMetaDatas);

}
