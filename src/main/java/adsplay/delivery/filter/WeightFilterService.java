package adsplay.delivery.filter;

import java.util.List;

import adsplay.common.model.CreativeData;

public interface WeightFilterService {

    CreativeData doFilter(List<CreativeData> creativeDatas);

}
