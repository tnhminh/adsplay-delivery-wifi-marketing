package adsplay.delivery.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import adsplay.common.model.CreativeData;
import adsplay.common.model.DateRange;
import adsplay.common.util.DateTimeUtil;
import adsplay.delivery.field.metadata.TargetEnum;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.filter.RedisPool;
import rfx.core.util.LogUtil;
import rfx.core.util.StringUtil;

public class BannerFilterServiceImpl implements FilterService {

    @Override
    public List<CreativeData> doFilterAndTargeting(List<TargetEnum> targetMetaDatas, List<CreativeData> creativeDatas, int placementId,
            Map<String, Object> params, Map<Integer, Integer> crtIdWeightPairs) {

        List<CreativeData> result = new ArrayList<CreativeData>();

        for (CreativeData creative : creativeDatas) {
            
            int crtId = creative.getCreativeId();

            crtIdWeightPairs.put(creative.getCreativeId(), creative.getWeight());

            String timeZone = "";
            try {
                boolean isValidMac = false;
                boolean isValidDateRange = false;
                boolean isValidPlacementId = true;
                boolean isAvailableBooking = false;

                isAvailableBooking = isAvailableBooking(creative.getFlightId(), creative.getTotalBookings(), creative.getDailyBookings(),
                        timeZone);

                if (!isAvailableBooking) {
                    LogUtil.i(Thread.currentThread().getName(), "\n crtId:" + crtId + ", \n isValidMac:" + isValidMac + ", \n isValidDateRange:"
                            + isValidDateRange + ", \n isAvailableBooking:" + isAvailableBooking, true);
                    continue;
                }

                // Iterator<PublisherData> pubIte =
                // creative.getPublishers().iterator();
                // while (pubIte.hasNext()) {
                // PublisherData pubObj = pubIte.next();
                // Iterator<Integer> pmIte =
                // pubObj.getPlacementIds().iterator();
                // while (pmIte.hasNext()) {
                // if (pmIte.next() == placementId) {
                // // Only 1 placement which is validate
                // isValidPlacementId = true;
                // timeZone = pubObj.getTimeZone();
                // break;
                // }
                // }
                // }
                //
                // if (!isValidPlacementId) {
                // continue;
                // }

                List<String> macAddressList = creative.getMacAddress();
                String macAddress = StringUtil.safeString(params.get("mac_address"), "");
                isValidMac = isTargetingOnMacAddress(macAddressList, macAddress, targetMetaDatas);

                if (!isValidMac) {
                    LogUtil.i(Thread.currentThread().getName(), "\n crtId:" + crtId + ", \n isValidMac:" + isValidMac + ", \n isValidDateRange:"
                            + isValidDateRange + ", \n isAvailableBooking:" + isAvailableBooking, true);
                    continue;
                }

                // Date Range Array
                List<DateRange> dateRanges = creative.getDateRanges();

                Iterator<DateRange> dateRangeIte = dateRanges.iterator();
                while (dateRangeIte.hasNext()) {
                    // Targeting on time
                    if (doTargetingOnTime(dateRangeIte, timeZone, targetMetaDatas)) {
                        isValidDateRange = true;
                    }
                }

                if (!isValidDateRange) {
                    LogUtil.i(Thread.currentThread().getName(), "\n crtId:" + crtId + ", \n isValidMac:" + isValidMac + ", \n isValidDateRange:"
                            + isValidDateRange + ", \n isAvailableBooking:" + isAvailableBooking, true);
                    continue;
                }

                LogUtil.i(Thread.currentThread().getName(), "\n crtId:" + crtId + ", \n isValidMac:" + isValidMac + ", \n isValidDateRange:"
                        + isValidDateRange + ", \n isAvailableBooking:" + isAvailableBooking, true);

                if (isValidPlacementId && isValidMac && isValidDateRange & isAvailableBooking) {
                    // Adding creative to final list
                    result.add(creative);
                }
            } catch (Exception e) {
                LogUtil.error(BannerFilterServiceImpl.class, "execute()",
                        "Error when filter and targeting wifi creative !!!" + creative.getCreativeId());
                LogUtil.error(e);
            }

        }
        return result;

    }

    @Override
    public boolean isIncludeDateOfWeek(List<Integer> dateOfWeeks, String timeZone) {
        if (dateOfWeeks.isEmpty()) {
            return true;
        }
        int currentDateOfWeek = StringUtil.safeParseInt(adsplay.common.util.DateTimeUtil.getCurrentDateOfWeek(timeZone, true));

        return dateOfWeeks.contains(currentDateOfWeek);
    }

    @Override
    public boolean isIncludeHour(List<Integer> hours, String timeZone) {
        if (hours.isEmpty()) {
            return true;
        }
        int currentHour = StringUtil.safeParseInt(DateTimeUtil.getCurrentHour(timeZone, true));
        return hours.contains(currentHour);
    }

    @Override
    public boolean isIncludeDateRange(String fromDate, String toDate, String timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.YYYY_MM_DD);

        String currentDate = DateTimeUtil.convertDateToStringFormat(new Date(), timeZone, DateTimeUtil.YYYY_MM_DD);
        Date dateAfterParsing = null;
        try {
            dateAfterParsing = dateFormat.parse(currentDate);
            if (dateAfterParsing.compareTo(dateFormat.parse(toDate)) <= 0 && dateAfterParsing.compareTo(dateFormat.parse(fromDate)) >= 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean isAvailableBooking(String flightId, int totalBooking, int dailyBooking, String timeZone) {

        // Current date Using UTC time - the reason is redis daily booking
        // using UTC.
        String dailyByDate = DateTimeUtil.getCurrentDateString(timeZone, DateTimeUtil.YYYYMMDD, true, 0, 0, 0);

        // Get from cache db
        Map<String, String> dbBookingVals = RedisCache.hget(BOOKING + flightId, RedisPool.BOOKING);
        String bkType = StringUtil.safeString(dbBookingVals.get(BOOKING_TYPE), "impression");

        String totalBookingKey = "";
        String dailyBookingKey = "";
        if (!bkType.isEmpty()) {
            totalBookingKey = BK + UNDERLINE + flightId + UNDERLINE + TOTAL + UNDERLINE + bkType;
            dailyBookingKey = BK + UNDERLINE + flightId + UNDERLINE + DAILY + UNDERLINE + dailyByDate + UNDERLINE + bkType;
        }

        // Note: Get tracking REAL-TIME value
        int totalRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(totalBookingKey, RedisPool.REALTIME_BOOKING), 0);
        int dailyRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(dailyBookingKey, RedisPool.REALTIME_BOOKING), 0);

        return isAvailable(dailyBooking, totalBooking, totalRealtimeBkVal, dailyRealtimeBkVal);

    }

    private boolean isAvailable(int dailyVal, int totalVal, int totalRealtimeBkVal, int dailyRealtimeBkVal) {
        if (dailyVal == 0) {
            return totalVal > totalRealtimeBkVal;
        } else {
            return totalVal > totalRealtimeBkVal && dailyVal > dailyRealtimeBkVal;
        }
    }

    @Override
    public boolean isTargetingOnMacAddress(List<String> macAddressList, String macParam, List<TargetEnum> targetedMetaDatas) {
        boolean isValidMac = false;
        if (macAddressList.isEmpty()) {
            isValidMac = true;
        } else {
            if (targetedMetaDatas.contains(TargetEnum.MAC_ADR)) {
                isValidMac = macAddressList.contains(macParam);
            } else {
                isValidMac = true;
            }
        }
        return isValidMac;
    }

    @Override
    public boolean doTargetingOnTime(Iterator<DateRange> dateRangeIte, String timezone, List<TargetEnum> targetedMetaDatas) {

        boolean isIncludeDateOfWeek = false;
        boolean isIncludeHour = false;
        boolean isIncludeDateRange = false;

        DateRange dateRangeField = dateRangeIte.next();
        String from = dateRangeField.getFrom();
        String to = dateRangeField.getTo();
        List<Integer> hours = dateRangeField.getHours();
        List<Integer> dateOfWeek = dateRangeField.getDateOfWeeks();

        if (hours.isEmpty()) {
            isIncludeHour = true;
        } else {
            if (!targetedMetaDatas.contains(TargetEnum.HOURS)) {
                isIncludeHour = true;
            } else {
                isIncludeHour = isIncludeHour(hours, timezone);
            }
        }

        if (dateOfWeek.isEmpty()) {
            isIncludeDateOfWeek = true;
        } else {
            if (!targetedMetaDatas.contains(TargetEnum.DATE_OF_WEEKS)) {
                isIncludeDateOfWeek = true;
            } else {
                isIncludeDateOfWeek = isIncludeDateOfWeek(dateOfWeek, timezone);
            }
        }

        if ("".equals(from) && "".equals(to)) {
            isIncludeDateRange = true;
        } else {
            if (!targetedMetaDatas.contains(TargetEnum.FROM_AND_TO)) {
                isIncludeDateRange = true;
            } else {
                isIncludeDateRange = isIncludeDateRange(from, to, timezone);
                if (!isIncludeDateRange) {
                    return false;
                }
            }
        }

        return isIncludeDateOfWeek && isIncludeDateRange && isIncludeHour;
    }

}
