package adsplay.delivery.job;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractDeliveryRequestJob {

    private String jobName = "default-name";

    private String jobType = "default-type";

    private Map<String, Object> paramAsMap = new HashMap<String, Object>();

    private int placementId = 0;

    public AbstractDeliveryRequestJob(String jobName, String jobType, Map<String, Object> paramAsMap, int placementId) {
        super();
        this.jobName = jobName;
        this.jobType = jobType;
        this.paramAsMap = paramAsMap;
        this.placementId = placementId;
    }

    public int getPlacementId() {
        return placementId;
    }

    public void setPlacementId(int placementId) {
        this.placementId = placementId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public Map<String, Object> getParamAsMap() {
        return paramAsMap;
    }

    public void setParamAsMap(Map<String, Object> paramAsMap) {
        this.paramAsMap = paramAsMap;
    }

}
