package adsplay.delivery.job;

public interface ExecuteJobService {

    String executeJob(AbstractDeliveryRequestJob job);

}
