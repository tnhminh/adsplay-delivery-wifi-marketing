package adsplay.delivery.request.job.executor;

import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import adsplay.delivery.redis.cache.DeliveryRequestTracking;
import adsplay.delivery.redis.cache.filter.RedisPool;
import rfx.core.util.LogUtil;

public class RequestJobExecutor {

    private static final int MAX_JOB_QUEUE = 10000;

    private static Timer m_timer = new Timer();

    private static LinkedBlockingQueue<Map<String, List<String>>> m_requestQueues = new LinkedBlockingQueue<Map<String, List<String>>>();

    static {
        initJob();
    }

    private static void initJob() {
        m_timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                try {
                    runJobQueue();
                } catch (Exception e) {
                    LogUtil.error(RequestJobExecutor.class, "initJob", "Error when runJobQueue.");
                    LogUtil.error(e);
                }
            }

        }, 500, 2000);
    }

    private static void runJobQueue() {
        LinkedBlockingQueue<Map<String, List<String>>> maxExecuteQueues = new LinkedBlockingQueue<Map<String, List<String>>>(MAX_JOB_QUEUE);

        if (m_requestQueues.isEmpty()) {
            return;
        }

        int i = 0;
        while (i < MAX_JOB_QUEUE && !m_requestQueues.isEmpty()) {
            try {
                maxExecuteQueues.offer(m_requestQueues.poll(5000, TimeUnit.MILLISECONDS));
            } catch (InterruptedException e) {
                LogUtil.error(RequestJobExecutor.class, "runJobQueue", "Error when polling request.");
                LogUtil.error(e);
            }
            i++;
        }

        if (!maxExecuteQueues.isEmpty()) {
            DeliveryRequestTracking.updateHListEvents(maxExecuteQueues, RedisPool.INV_DATA);
            LogUtil.i(RequestJobExecutor.class, "Update request to redis, size:" + maxExecuteQueues.size(), true);
            maxExecuteQueues.clear();
        }
    }

    public static void executeRequestJob(Map<String, List<String>> event) {
        boolean isSuccess = false;
        if (event != null && !event.isEmpty()) {
            isSuccess = m_requestQueues.offer(event);
            if (!isSuccess) {
                LogUtil.i(RequestJobExecutor.class, "Adding request job fail !!!", true);
            }
        }
    }
}
