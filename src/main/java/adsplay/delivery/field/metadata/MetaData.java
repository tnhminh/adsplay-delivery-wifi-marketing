package adsplay.delivery.field.metadata;

import java.util.List;

public interface MetaData {
    List<TargetEnum> getTargetingEnums();

}
