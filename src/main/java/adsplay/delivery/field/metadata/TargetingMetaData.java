package adsplay.delivery.field.metadata;

import java.util.ArrayList;
import java.util.List;

public class TargetingMetaData implements MetaData {

    @Override
    public List<TargetEnum> getTargetingEnums() {
        List<TargetEnum> targetedEnums = new ArrayList<TargetEnum>();
        targetedEnums.add(TargetEnum.MAC_ADR);
        targetedEnums.add(TargetEnum.HOURS);
        targetedEnums.add(TargetEnum.FROM_AND_TO);
        targetedEnums.add(TargetEnum.DATE_OF_WEEKS);
        return targetedEnums;
    }

}
