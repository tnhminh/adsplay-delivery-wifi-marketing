package adsplay.delivery.properties;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import rfx.core.util.FileUtils;
import rfx.core.util.LogUtil;

public class DeliveryConfiguration {

    private static final int ONE_HOUR = 3600 * 1000;

    private static final String CONFIG_PATH = "/" + "configs/" + "delivery.properties";

    private static Properties properties = new Properties();

    private static Timer timer = new Timer();
    
    private DeliveryConfiguration() {
    }

    static {
        init();
        if (!Boolean.valueOf(DeliveryConfiguration.getProperty("isProduction", "true"))) {
            timer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    init();
                }
            }, 500, (ONE_HOUR) / 2);
        }
    }

    private static void init() {
        DataInputStream streamConfigFile = FileUtils.readFileAsStream(CONFIG_PATH);
        try {
            properties.load(streamConfigFile);
        } catch (IOException e) {
            LogUtil.error(e);
        }
    }

    public static String getProperty(String key) {
        return getProperty(key, "");
    }

    public static String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    public static void main(String[] args) {
        System.out.println(properties.getProperty("placement.ad_view.field"));
    }
}
