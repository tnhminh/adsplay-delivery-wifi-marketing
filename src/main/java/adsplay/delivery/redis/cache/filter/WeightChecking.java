package adsplay.delivery.redis.cache.filter;

import java.util.List;
import java.util.Map;

public interface WeightChecking {

    List<Integer> checkingWeight(Map<List<Integer>, Integer> crtIdWeightPair, List<Integer> crts, List<Integer> allCrtIds, Map<String, Object> paramsAsMap);

}
