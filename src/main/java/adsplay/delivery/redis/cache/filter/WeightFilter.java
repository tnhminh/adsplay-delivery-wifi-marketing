package adsplay.delivery.redis.cache.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import adsplay.common.util.ParamUtils;
import adsplay.common.util.ParserUtils;
import adsplay.delivery.properties.MessageProperties;
import adsplay.delivery.redis.cache.RedisCache;
import rfx.core.util.StringUtil;

public class WeightFilter {
    
    private static final String DASH = MessageProperties.getProperty("general.dash", "");

    private static final String WEIGHT_PREFIX = MessageProperties.getProperty("general.weight_prefix", "");

    public static List<Integer> weightChecking(Map<List<Integer>, Integer> crtIdWeightPair, List<Integer> crtFreqIds, List<Integer> allCrtIds,
            Map<String, Object> paramsAsMap) {

        if (crtFreqIds.isEmpty()) {
            return new ArrayList<Integer>();
        }
        
        List<Integer> result = new ArrayList<Integer>();
        
        List<Integer> wValList = new ArrayList<Integer>();

        // Sort creative id to avoid random element in this list. This will go
        // wrong case
        Collections.sort(crtFreqIds);

        String[] separator = new String[2];

        // Build key
        StringBuilder wKey = buildKeyAndValOfWeight(crtIdWeightPair, crtFreqIds, wValList, separator);

        // get weight value from key
        String crtWeightArr = StringUtil.safeString(RedisCache.getCreativeWeight(wKey.toString()), "");
        String[] crtWeightRedisArr = ParserUtils.convertStringToArray(crtWeightArr);
        paramsAsMap.put(ParamUtils.WEIGHT_CREATIVES_KEY, wKey.toString());

        if (crtWeightRedisArr == null || crtWeightRedisArr.length == 0 || "[]".equals(crtWeightArr)) {
            // set new initial key/value
            String wValStr = buildValue(wValList, separator).toString();
            RedisCache.setCreativeWeight(wKey.toString(), wValStr);
            Integer finalWeightCrtId = 0;
            if (!wValList.isEmpty()){
                finalWeightCrtId = wValList.get(0);
            }
            paramsAsMap.put("final_weight_crt", finalWeightCrtId.intValue());
            result.add(finalWeightCrtId);
        } else {
            List<String> crtRedisArr = new ArrayList<String>(Arrays.asList(crtWeightRedisArr));
            Collections.shuffle(crtRedisArr);
            RedisCache.setCreativeWeight(wKey.toString(), buildValue(crtRedisArr, separator).toString());
            Integer finalWeightCrtId = Integer.valueOf(crtRedisArr.get(0));
            result.add(finalWeightCrtId);
            paramsAsMap.put("final_weight_crt", finalWeightCrtId.intValue());
        }
        return result;
    }

    private static String buildValue(List<? extends Object> wVals, String[] separator) {
        if (wVals.isEmpty()){
            return "";
        }
        // Create chao of weight vals
        Collections.shuffle(wVals);

        // =====
        StringBuilder wVal = new StringBuilder();
        separator[1] = "";

        wVal.append("[");
        wVals.forEach(crtClone -> {
            wVal.append(separator[1]);
            if (crtClone instanceof Integer){
                wVal.append((Integer)crtClone);
            } else if (crtClone instanceof String){
                wVal.append((String)crtClone);
            }
            separator[1] = ", ";
        });
        wVal.append("]");
        return wVal.toString();
    }

    private static StringBuilder buildKeyAndValOfWeight(Map<List<Integer>, Integer> crtIdWeightPair, List<Integer> crtIds, List<Integer> wVals,
            String[] separator) {
        separator[0] = "";
        StringBuilder wKey = new StringBuilder();
        wKey.append(WEIGHT_PREFIX);
        crtIds.forEach(crtId -> {
            wKey.append(separator[0]);
            wKey.append(crtId);
            separator[0] = DASH;

            for (Entry<List<Integer>, Integer> entry : crtIdWeightPair.entrySet()) {
                int weight = entry.getValue();
                for (Integer id : entry.getKey()) {
                    if (id.equals(crtId)) {
                        int k = 0;
                        while (k < weight) {
                            wVals.add(id);
                            k++;
                        }
                    }
                }
            }
        });
        return wKey;
    }
    
    public static void main(String[] args) {
        Map<List<Integer>, Integer> crtIdWeightPair = new HashMap<List<Integer>, Integer>();
        List<Integer> crtIds = new ArrayList<Integer>();
        crtIds.add(1000);
        crtIds.add(3000);
        crtIds.add(5000);
        
        // All crt
        crtIdWeightPair.put(crtIds, 6);
        
        List<Integer> crtFreqIds = new ArrayList<Integer>();
        crtFreqIds.add(1000);
        crtFreqIds.add(5000);
        
        System.out.println(weightChecking(crtIdWeightPair, crtFreqIds, new ArrayList<Integer>(), new HashMap<>()));
    }
}
