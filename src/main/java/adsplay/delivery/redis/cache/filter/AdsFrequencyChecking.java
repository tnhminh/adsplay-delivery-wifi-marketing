package adsplay.delivery.redis.cache.filter;

import java.util.List;
import java.util.Map;

public interface AdsFrequencyChecking {

    List<Integer> checkingAdsFrequency(String uuid, Map<List<Integer>, List<Integer>> crtsFrequencyPair);
    
    boolean isNeedToCheckAdsFreq();

}
