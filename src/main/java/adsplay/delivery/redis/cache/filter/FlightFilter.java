package adsplay.delivery.redis.cache.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adsplay.common.util.DateTimeUtil;
import adsplay.common.util.ParserUtils;
import adsplay.delivery.function.Targeting;
import adsplay.delivery.redis.cache.RedisCache;
import adsplay.delivery.redis.cache.model.DateRange;
import rfx.core.util.StringUtil;

public abstract class FlightFilter implements Targeting {

    public boolean isAvailableBooking(String flightId, String timeZone) {

        // Current date Using UTC time - the reason is redis daily booking
        // using UTC.
        String dailyByDate = DateTimeUtil.getCurrentDateString(timeZone, DateTimeUtil.YYYYMMDD, true, 0, 0, 0);

        // Get from cache db
        Map<String, String> dbBookingVals = RedisCache.hget(BOOKING + flightId, RedisPool.BOOKING);
        String bkType = StringUtil.safeString(dbBookingVals.get(BOOKING_TYPE), "impression");
        int dailyVal = StringUtil.safeParseInt(dbBookingVals.get(DAILY), 0);
        int totalVal = StringUtil.safeParseInt(dbBookingVals.get(TOTAL), 0);

        //
        String totalBookingKey = "";
        String dailyBookingKey = "";
        if (!bkType.isEmpty()) {
            totalBookingKey = BK + UNDERLINE + flightId + UNDERLINE + TOTAL + UNDERLINE + bkType;
            dailyBookingKey = BK + UNDERLINE + flightId + UNDERLINE + DAILY + UNDERLINE + dailyByDate + UNDERLINE + bkType;
        }

        // Note: Get tracking REAL-TIME value
        int totalRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(totalBookingKey, RedisPool.REALTIME_BOOKING), 0);
        int dailyRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(dailyBookingKey, RedisPool.REALTIME_BOOKING), 0);

        return isAvailable(dailyVal, totalVal, totalRealtimeBkVal, dailyRealtimeBkVal);
    }
    
    public boolean isAvailableBookingForWifi(String flightId, String timeZone) {

        // Current date Using UTC time - the reason is redis daily booking
        // using UTC.
        String dailyByDate = DateTimeUtil.getCurrentDateString(timeZone, DateTimeUtil.YYYYMMDD, true, 0, 0, 0);

        // Get from cache db
        Map<String, String> dbBookingVals = RedisCache.hget(BOOKING + flightId, RedisPool.BOOKING);
        String bkType = StringUtil.safeString(dbBookingVals.get(BOOKING_TYPE), "impression");
        int dailyVal = StringUtil.safeParseInt(dbBookingVals.get(DAILY), 0);
        int totalVal = StringUtil.safeParseInt(dbBookingVals.get(TOTAL), 0);

        //
        String totalBookingKey = "";
        String dailyBookingKey = "";
        if (!bkType.isEmpty()) {
            totalBookingKey = BK + UNDERLINE + flightId + UNDERLINE + TOTAL + UNDERLINE + bkType;
            dailyBookingKey = BK + UNDERLINE + flightId + UNDERLINE + DAILY + UNDERLINE + dailyByDate + UNDERLINE + bkType;
        }

        // Note: Get tracking REAL-TIME value
        int totalRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(totalBookingKey, RedisPool.REALTIME_BOOKING), 0);
        int dailyRealtimeBkVal = StringUtil.safeParseInt(RedisCache.get(dailyBookingKey, RedisPool.REALTIME_BOOKING), 0);

        return isAvailable(dailyVal, totalVal, totalRealtimeBkVal, dailyRealtimeBkVal);
    }

    private boolean isAvailable(int dailyVal, int totalVal, int totalRealtimeBkVal, int dailyRealtimeBkVal) {
        if (dailyVal == 0) {
            return totalVal > totalRealtimeBkVal;
        } else {
            return totalVal > totalRealtimeBkVal && dailyVal > dailyRealtimeBkVal;
        }
    }

    public boolean isIncludeHour(String hours, String timeZone) {
        if (checkingEmptyVal(hours)) {
            return true;
        }
        String[] arr = ParserUtils.convertStringToArray(hours);

        List<String> fParams = Arrays.asList(arr);
        String currentHour = DateTimeUtil.getCurrentHour(timeZone, true);
        return fParams.contains(currentHour);
    }

    private boolean checkingEmptyVal(String string) {
        return string.isEmpty() || "null".equals(string) || "None".equals(string);
    }

    public boolean isIncludeDateOfWeek(String dateOfWeeks, String timeZone) {
        if (checkingEmptyVal(dateOfWeeks)) {
            return true;
        }
        String[] arr = ParserUtils.convertStringToArray(dateOfWeeks);
        List<String> fParams = Arrays.asList(arr);
        String currentDateOfWeek = DateTimeUtil.getCurrentDateOfWeek(timeZone, true);

        return fParams.contains(currentDateOfWeek);
    }

    public boolean isIncludeDateRange(String dateRanges, String timeZone) {
        if (checkingEmptyVal(dateRanges)) {
            return true;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        List<DateRange> listDateRanges = gson.fromJson(dateRanges, new TypeToken<List<DateRange>>() {
        }.getType());
        Date date = DateTimeUtil.getCurrentDateTimeZone(timeZone, true).getTime();
        boolean isIncluded[] = new boolean[1];
        isIncluded[0] = false;

        Iterator<DateRange> iterator = listDateRanges.iterator();
        while (iterator.hasNext()) {
            if (isIncluded[0] == true)
                break;
            DateRange dateRange = iterator.next();
            if (date.before(new Date(dateRange.getTo())) && date.after(new Date(dateRange.getFrom()))) {
                isIncluded[0] = true;
            }
        }
        return isIncluded[0];
    }

    public boolean isIncludeDateRange(String fromDate, String toDate, String timeZone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.YYYY_MM_DD);

        String currentDate = DateTimeUtil.convertDateToStringFormat(new Date(), timeZone, DateTimeUtil.YYYY_MM_DD);
        Date dateAfterParsing = null;
        try {
            dateAfterParsing = dateFormat.parse(currentDate);
            if (dateAfterParsing.compareTo(dateFormat.parse(toDate)) <= 0 && dateAfterParsing.compareTo(dateFormat.parse(fromDate)) >= 0) {
                return true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isIncludeDateRangeWithinComparing(String dateRanges, String timeZone) {
        if (checkingEmptyVal(dateRanges)) {
            return true;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.YYYY_MM_DD);

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String currentDate = DateTimeUtil.convertDateToStringFormat(new Date(), timeZone, DateTimeUtil.YYYY_MM_DD);

        Date dateAfterParsing = null;
        boolean isIncluded[] = new boolean[1];
        try {
            dateAfterParsing = dateFormat.parse(currentDate);

            List<DateRange> listDateRanges = gson.fromJson(dateRanges, new TypeToken<List<DateRange>>() {
            }.getType());

            isIncluded[0] = false;
            Iterator<DateRange> iterator = listDateRanges.iterator();
            while (iterator.hasNext()) {
                if (isIncluded[0] == true)
                    break;
                DateRange dateRange = iterator.next();
                if (dateAfterParsing.compareTo(dateFormat.parse(dateRange.getTo())) <= 0
                        && dateAfterParsing.compareTo(dateFormat.parse(dateRange.getFrom())) >= 0) {
                    isIncluded[0] = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isIncluded[0];
    }

    public static void main(String[] args) throws ParseException {

    }
}
