package adsplay.delivery.redis.cache;

import java.util.HashMap;
import java.util.Map;

import com.adsplay.sentry.handler.SentryHanlder;

import adsplay.delivery.properties.MessageProperties;
import adsplay.delivery.redis.cache.filter.RedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.LogUtil;

public class RedisCache {

    private static final String DELIVEY_REDIS_CACHE = MessageProperties.getProperty("redis.source.name.delivery.cache", "");

    private static final String PROVINCES_MAPPING_DATA = MessageProperties.getProperty("redis.source.name.provinces_mapping_data", "");

    private static final String REALTIME_BOOKING = MessageProperties.getProperty("redis.source.name.realtime_booking", "");

    private static final String API_DATA = MessageProperties.getProperty("redis.source.name.api_data", "");

    private static final String FLIGHT_DATA = MessageProperties.getProperty("redis.source.name.flight_data", "");

    private static final String CREATIVE_DATA = MessageProperties.getProperty("redis.source.name.creative_data", "");

    private static final String PLACEMENT_DATA = MessageProperties.getProperty("redis.source.name.placement_data", "");

    private static final String BOOKING_DATA = MessageProperties.getProperty("redis.source.name.booking_data", "");

    private static final String CREATIVE_WEIGHT_DATA = MessageProperties.getProperty("redis.source.name.creative_weight_data", "");
    
    private static final String ADS_FREQUENCY = MessageProperties.getProperty("redis.source.name.ads_frequency", "");
    
    private static final String LOCATION_IP_ADDR = MessageProperties.getProperty("redis.source.name.location_ip_addr", "");

    // Get redis pool in redis.config, need to share in all separated port
    static ShardedJedisPool creativeWeightPool = RedisConfigs.load().get(CREATIVE_WEIGHT_DATA).getShardedJedisPool();

    static ShardedJedisPool bookingPool = RedisConfigs.load().get(BOOKING_DATA).getShardedJedisPool();

    static ShardedJedisPool placmentPool = RedisConfigs.load().get(PLACEMENT_DATA).getShardedJedisPool();

    static ShardedJedisPool creativeData = RedisConfigs.load().get(CREATIVE_DATA).getShardedJedisPool();

    static ShardedJedisPool flightData = RedisConfigs.load().get(FLIGHT_DATA).getShardedJedisPool();
    
    static ShardedJedisPool apiData = RedisConfigs.load().get(API_DATA).getShardedJedisPool();
    
    static ShardedJedisPool realtimeBooking = RedisConfigs.load().get(REALTIME_BOOKING).getShardedJedisPool();
    
    static ShardedJedisPool adsFrequency = RedisConfigs.load().get(ADS_FREQUENCY).getShardedJedisPool();
    
    static ShardedJedisPool locationIpAddr = RedisConfigs.load().get(LOCATION_IP_ADDR).getShardedJedisPool();
    
    static ShardedJedisPool provincesMappingData = RedisConfigs.load().get(PROVINCES_MAPPING_DATA).getShardedJedisPool();
    
    static ShardedJedisPool deliveryRedisCache = RedisConfigs.load().get(DELIVEY_REDIS_CACHE).getShardedJedisPool();

    public static void init(){
    }
    
    public static void incrBy(final String key, int expiredTime, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;
            if (redisPool.equals(RedisPool.ADS_FREQUENCY)) {
                redisPoolProxy = adsFrequency;
            }
            
            new RedisCommand<Void>(redisPoolProxy) {
                @Override
                protected Void build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        p.incr(key);
                        p.expire(key, expiredTime);
                        p.sync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return null;
                }
            }.execute();
        }catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("incrBy", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache !!!",
                    e, false);
            LogUtil.error(e);
        }
    }
    
    public static Map<String, String> hget(final String key, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;
            if (redisPool.equals(RedisPool.BOOKING)) {
                redisPoolProxy = bookingPool;
            } else if (redisPool.equals(RedisPool.CREATIVE)) {
                redisPoolProxy = creativeData;
            } else if (redisPool.equals(RedisPool.FLIGHT)) {
                redisPoolProxy = flightData;
            } else if (redisPool.equals(RedisPool.PLACEMENT)) {
                redisPoolProxy = placmentPool;
            } else if (redisPool.equals(RedisPool.FLIGHT_WEIGHT)) {
                redisPoolProxy = creativeWeightPool;
            } else if (redisPool.equals(RedisPool.REALTIME_BOOKING)) {
                redisPoolProxy = realtimeBooking;
            }  else if (redisPool.equals(RedisPool.DELIVERY_REDIS_CACHE)) {
                redisPoolProxy = deliveryRedisCache;
            }
            Map<String, String> result = new RedisCommand<Map<String, String>>(redisPoolProxy) {
                @Override
                protected Map<String, String> build() {
                    try {
                        return jedis.hgetAll(key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return new HashMap<>();
                }
            }.execute();
            return result;
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("hget", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache !!!",
                    e, false);
            LogUtil.error(e);
        }
        return new HashMap<>();
    }
    
    
    public static String hget(final String key, final String fieldName, RedisPool redisPool) {
        try {
            ShardedJedisPool redisPoolProxy = null;
            if (redisPool.equals(RedisPool.BOOKING)) {
                redisPoolProxy = bookingPool;
            } else if (redisPool.equals(RedisPool.CREATIVE)) {
                redisPoolProxy = creativeData;
            } else if (redisPool.equals(RedisPool.FLIGHT)) {
                redisPoolProxy = flightData;
            } else if (redisPool.equals(RedisPool.PLACEMENT)) {
                redisPoolProxy = placmentPool;
            } else if (redisPool.equals(RedisPool.FLIGHT_WEIGHT)) {
                redisPoolProxy = creativeWeightPool;
            } else if (redisPool.equals(RedisPool.REALTIME_BOOKING)) {
                redisPoolProxy = realtimeBooking;
            }
            String result = new RedisCommand<String>(redisPoolProxy) {
                @Override
                protected String build() {
                    try {
                        return jedis.hget(key, fieldName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return "";
                }
            }.execute();
            return result;
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("hget", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache !!!",
                    e, false);
            LogUtil.error(e);
        }
        return "";
    }

    public static String get(final String key, RedisPool poolName) {
        String result = "";
        ShardedJedisPool redisPoolProxy = null;
        try {
            if (poolName.equals(RedisPool.BOOKING)) {
                redisPoolProxy = bookingPool;
            } else if (poolName.equals(RedisPool.CREATIVE)) {
                redisPoolProxy = creativeData;
            } else if (poolName.equals(RedisPool.FLIGHT)) {
                redisPoolProxy = flightData;
            } else if (poolName.equals(RedisPool.PLACEMENT)) {
                redisPoolProxy = placmentPool;
            } else if (poolName.equals(RedisPool.FLIGHT_WEIGHT)) {
                redisPoolProxy = creativeWeightPool;
            } else if (poolName.equals(RedisPool.API_DATA)) {
                redisPoolProxy = apiData;
            } else if (poolName.equals(RedisPool.REALTIME_BOOKING)) {
                redisPoolProxy = realtimeBooking;
            } else if (poolName.equals(RedisPool.ADS_FREQUENCY)) {
                redisPoolProxy = adsFrequency;
            } else if (poolName.equals(RedisPool.LOCATION_IP_ADDR)) {
                redisPoolProxy = locationIpAddr;
            } else if (poolName.equals(RedisPool.PROVINCES_DATA)) {
                redisPoolProxy = provincesMappingData;
            } else if (poolName.equals(RedisPool.DELIVERY_REDIS_CACHE)) {
                redisPoolProxy = deliveryRedisCache;
            }

            result = new RedisCommand<String>(redisPoolProxy) {
                @Override
                protected String build() {
                    try {
                        return jedis.get(key);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return "";
                }
            }.execute();
            return result;
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("get", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache !!!",
                    e, false);        
            LogUtil.error(e);
        }
        return "";

    }

    public static String getCreativeWeight(final String key) {
        try {
            String result = new RedisCommand<String>(creativeWeightPool) {
                @Override
                protected String build() {
                    try {
                        String result = jedis.get(key);
                        return result;
                    } catch (Exception e) {
                        return "";
                    } finally {
                        jedis.close();
                    }
                }
            }.execute();
            return result;
        }catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("getFlightWeight", "minhtnh2@fpt.com.vn", null, null, "Error when getFlightWeight from redis cache !!!",
                    e, false);        
            LogUtil.error(e);
        }
        return "";
       
    }

    public static void setCreativeWeight(final String key, final String value) {
        try {
            new RedisCommand<Void>(creativeWeightPool) {
                @Override
                protected Void build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        p.set(key, value);
                        p.sync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return null;
                }
            }.execute();
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("setFlightWeight", "minhtnh2@fpt.com.vn", null, null,
                    "Error when setFlightWeight to redis cache !!!", e, false);
            LogUtil.error(e);
        }
    }

    public static void delCreativeWeight(final String key) {
        try {
            new RedisCommand<Void>(creativeWeightPool) {
                @Override
                protected Void build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        p.del(key);
                        p.sync();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        jedis.close();
                    }
                    return null;
                }
            }.execute();
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("delFlightWeight", "minhtnh2@fpt.com.vn", null, null,
                    "Error when delFlightWeight to redis cache !!!", e, false);
            LogUtil.error(e);
        }

    }

    public static boolean insertDummyData() {
        boolean commited = false;
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(creativeData) {
                @Override
                protected Boolean build() throws JedisException {
                    Pipeline p = jedis.pipelined();
                    Map<String, String> startTimeMap = new HashMap<String, String>();
                    startTimeMap.put("type", "mid_roll");
                    startTimeMap.put("value", "00:00:10");

                    Map<String, String> creativeMap = new HashMap<String, String>();
                    creativeMap.put("source", "http://cdn.adsplay.net/2018/01/video.mp4");
                    creativeMap.put("landing_page", "https://www.vinamilk.com.vn");
                    creativeMap.put("skip_time", "00:00:05");
                    creativeMap.put("duration", "00:00:30");
                    creativeMap.put("flightID", "20");
                    creativeMap.put("start_time", startTimeMap.toString());
                    p.hmset("creative_12", creativeMap);

                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commited;
    }

    public static void main(String[] args) {
        // insertDummyData();
        // Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        // gson.toJson(getCacheByCache("creative_12"));
    }
}
