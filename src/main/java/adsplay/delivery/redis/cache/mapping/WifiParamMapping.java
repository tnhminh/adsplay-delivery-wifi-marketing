package adsplay.delivery.redis.cache.mapping;

import java.util.List;
import java.util.Map;

import adsplay.common.util.ParamUtils;
import adsplay.delivery.function.Mapping;
import io.vertx.core.MultiMap;
import rfx.core.util.StringUtil;

public class WifiParamMapping implements Mapping {

    @Override
    public void fillParamsMapping(MultiMap params, List<String> pmIds, Map<String, Object> paramsAsMap, Map<String, Object> additionalInfos) {

    }

    @Override
    public void fillParamsMapping(MultiMap params, String remoteIpAddr, List<String> pmIds, Map<String, Object> paramsAsMap,
            Map<String, Object> additionalInfos) {
        String uuid = StringUtil.safeString(params.get("uuid"), "");
        String mac_address = StringUtil.safeString(params.get("mac_address"), "");
        String ad_type = StringUtil.safeString(params.get("ad_type"), "");

        paramsAsMap.put(ParamUtils.UUID, uuid);
        paramsAsMap.put("mac_address", mac_address);
        paramsAsMap.put("ad_type", ad_type);

    }

}
