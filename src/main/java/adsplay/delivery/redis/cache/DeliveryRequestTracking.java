package adsplay.delivery.redis.cache;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;

import com.adsplay.sentry.handler.SentryHanlder;

import adsplay.delivery.redis.cache.filter.RedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.LogUtil;

public class DeliveryRequestTracking {
    
    static ShardedJedisPool inventoryDataPool = RedisConfigs.load().get("inventoryData").getShardedJedisPool();

    private static final String SUMMARY = "summary";

    public static final int ONE_DAY = 86400;

    public static final int AFTER_15_DAYS = ONE_DAY * 15;
    public static final int AFTER_30_DAYS = ONE_DAY * 30;
    public static final int AFTER_60_DAYS = ONE_DAY * 60;

    /**
     * 
     * @param prefix
     * @param unixtime
     * @param events
     * @param withSummary
     * @return
     */
    public static boolean updateEvent(final String[] events, boolean withSummary, RedisPool redisPool) {
        boolean commited = false;
        if (events.length == 0) {
            return false;
        }

        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(RedisPool.INV_DATA)) {
            redisPoolProxy = inventoryDataPool;
        }
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(redisPoolProxy) {
                @Override
                protected Boolean build() throws JedisException {

                    Pipeline p = jedis.pipelined();
                    for (String event : events) {
                        try {
                            if (withSummary) {
                                p.incr(SUMMARY);
                            }
                            p.incr(event);
                            //p.expire(event, AFTER_30_DAYS);
                        } catch (Exception e) {
                            SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null, "Error when updateEvent to redis cache !!!",
                                    e, false);
                        } finally {
                            jedis.close();
                        }
                    }
                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            LogUtil.error(DeliveryRequestTracking.class, "updateEvent function", "Error when updateEvent to redis cache !!!");
            LogUtil.error(e);
            SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null, "Error when updateEvent to redis cache !!!",
                    e, false);
        }
        return commited;
    }

    /**
     * 
     * @param prefix
     * @param unixtime
     * @param events
     * @param withSummary
     * @return
     */
    public static boolean updateHEvent(final Map<String, List<String>> mapEvents, RedisPool redisPool) {
        boolean commited = false;
        if (mapEvents.isEmpty()) {
            return false;
        }

        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(RedisPool.INV_DATA)) {
            redisPoolProxy = inventoryDataPool;
        }
        
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(redisPoolProxy) {
                @Override
                protected Boolean build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        for (Entry<String, List<String>> event : mapEvents.entrySet()) {
                            String key = event.getKey();
                            List<String> fields = event.getValue();
                            fields.forEach(field -> {
                                p.hincrBy(key, field, 1L);
                                // p.expire(key, AFTER_30_DAYS);
                            });
                        }
                        p.sync();
                        return true;
                    } catch (Exception e) {
                        SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null,
                                "Error when updateHEvent to redis cache !!!", e, false);
                        LogUtil.error(DeliveryRequestTracking.class, "updateHEvent function",
                                "Error when updateHEvent to redis cache !!!" + ", at " + redisPool);
                        LogUtil.error(e);
                    } finally {
                        jedis.close();
                    }
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache with mapEvents:" + mapEvents,
                    e, false);
            LogUtil.error(DeliveryRequestTracking.class, "updateHEvent function",
                    "Error when updateHEvent to redis cache with mapEvents:" + mapEvents + ", at " + redisPool);
            LogUtil.error(e);
        }
        return commited;
    }
    
    public static boolean updateHListEvents(final LinkedBlockingQueue<Map<String, List<String>>> mapEventList, RedisPool redisPool) {
        boolean commited = false;
        if (mapEventList.isEmpty()) {
            return false;
        }

        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(RedisPool.INV_DATA)) {
            redisPoolProxy = inventoryDataPool;
        }
        
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(redisPoolProxy) {
                @Override
                protected Boolean build() {
                    try {
                        Pipeline p = jedis.pipelined();
                        for (Map<String, List<String>> mapEvents : mapEventList){
                            for (Entry<String, List<String>> event : mapEvents.entrySet()) {
                                String key = event.getKey();
                                List<String> fields = event.getValue();
                                fields.forEach(field -> {
                                    p.hincrBy(key, field, 1L);
                                });
                            }
                        }
                        p.sync();
                        return true;
                    } catch (Exception e) {
                        SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null,
                                "Error when updateHEvent to redis cache !!!", e, false);
                        LogUtil.error(DeliveryRequestTracking.class, "updateHEvent function",
                                "Error when updateHEvent to redis cache !!!" + ", at " + redisPool);
                        LogUtil.error(e);
                    } finally {
                        jedis.close();
                    }
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            SentryHanlder.sendErrorToSentryLog("updateEvent", "minhtnh2@fpt.com.vn", null, null, "Error when updateHEvent to redis cache with mapListEvents:" + mapEventList,
                    e, false);
            LogUtil.error(DeliveryRequestTracking.class, "updateHEvent function",
                    "Error when updateHEvent to redis cache with mapListEvents:" + mapEventList + ", at " + redisPool);
            LogUtil.error(e);
        }
        return commited;
    }

    public static void main(String[] args) {

    }
}
