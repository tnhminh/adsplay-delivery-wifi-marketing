package adsplay.delivery.redis.cache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import adsplay.delivery.redis.cache.filter.RedisPool;
import rfx.core.util.LogUtil;

public class ExtraDataMidCache {

    private static ExtraDataMidCache cache = new ExtraDataMidCache();

    private static Map<String, String> extraMidFullAdCache = new HashMap<String, String>();

    private static Timer timer = new Timer("Extra-Midroll-Cache-Task-Every-30s");

    public static final int MID_120 = 7200;
    
    public static final int MID_1 = 60;

    public static final int PREROLL = 0;

    static {
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                initExtraMidrollCache();
            }
        }, 0, 30000);
    }

    private ExtraDataMidCache() {

    }

    public static ExtraDataMidCache getInstance() {
        return cache;
    }

    private static void initExtraMidrollCache() {
        LogUtil.i(ExtraDataMidCache.class, "Starting initialize extra data mid cache !!!", true);
        // Full extra mid data cache
        Map<String, String> configData = RedisCache.hget("config.extra.all.mid.cache", RedisPool.DELIVERY_REDIS_CACHE);
        boolean isClearCache = Boolean.valueOf(configData.get("isClearCache"));
        if (isClearCache){
            LogUtil.i(ExtraDataMidCache.class, "Clearing extra data mid cache !!!", true);
            extraMidFullAdCache.clear();
        } 
        if (configData!= null && configData.size() > 0) {
            // Loop key to find extra mid types
            for (Entry<String, String> extraMid : configData.entrySet()) {
                List<String> crtIds = new ArrayList<String>(Arrays.asList(extraMid.getValue().replaceAll("\\s+", "").split(",")));
                String everyMin = extraMid.getKey().replace("every", "").replace("min", "");
                for (String crtId : crtIds) {
                    extraMidFullAdCache.put(crtId, everyMin);
                }
            }
        }
    }

    public String getMinExtraData(String crtId) {
        return extraMidFullAdCache.get(crtId);
    }

    public static void main(String[] args) {
        System.out.println();
    }
}
