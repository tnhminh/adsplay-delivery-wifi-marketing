package adsplay.delivery.build.result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import adsplay.common.model.CreativeData;
import adsplay.common.model.LogAd;
import adsplay.common.model.WifiBannerModel;
import adsplay.common.util.DateTimeUtil;
import adsplay.common.util.ParamUtils;
import io.vertx.core.impl.StringEscapeUtils;
import rfx.core.util.StringUtil;

public class BannerBuildingResultService implements BuildingResultService {

    private static final String EXTERNAL = "external";

    @Override
    public String buildingResult(CreativeData data, Map<String, Object> paramAsMap, int placementId) {
        String finalResult = "";

        int creativeId = StringUtil.safeParseInt(data.getCreativeId());
        int flightId = StringUtil.safeParseInt(data.getFlightId());
        int campaignId = StringUtil.safeParseInt(data.getCampaignId());
        String buyType = StringUtil.safeString(data.getBuyType(), "no-buy-type");
        String displayType = StringUtil.safeString(data.getDisplayType(), "no-display-type");
        String creativeType = StringUtil.safeString(data.getCreativeType(), "no-creative-type");
        String creativeName = StringUtil.safeString(data.getCreativeName(), "no-creative-name");
        String landingPage = StringUtil.safeString(data.getLandingPage(), "");
        String sourcesString = StringUtil.safeString(data.getSourceName(), "").replace("[", "").replace("]", "").replace("'", "")
                .replaceAll("\\s+", "");
        String width = StringUtil.safeString(data.getWidth());
        String height = StringUtil.safeString(data.getHeight());

        String macArr = StringUtil.safeString(paramAsMap.get("mac_address"), "no-mac");
        String uuid = StringUtil.safeString(paramAsMap.get(ParamUtils.UUID), "");
        String adType = StringUtil.safeString(paramAsMap.get(ParamUtils.AD_TYPE), "");

        List<String> sources = null;
        if (data != null) {

            List<LogAd> logAds = new ArrayList<LogAd>();
            List<String> impTrackings = new ArrayList<String>();
            impTrackings.add(LOG_PREFIX + "event=impression" + "&creative_id=" + creativeId + "&flight_id=" + flightId + "&campaign_id="
                    + campaignId + "&placement_id=" + placementId + "&mac_address=" + macArr + "&buy_type=" + buyType + "&uuid=" + uuid
                    + "&cb=" + DateTimeUtil.currentUnixTimestamp() + "&ad_type=" + adType);
            List<String> clickTrackings = new ArrayList<String>();
            clickTrackings.add(LOG_PREFIX + "event=click" + "&creative_id=" + creativeId + "&flight_id=" + flightId + "&campaign_id="
                    + campaignId + "&placement_id=" + placementId + "&mac_address=" + macArr + "&buy_type=" + buyType + "&uuid=" + uuid
                    + "&cb=" + DateTimeUtil.currentUnixTimestamp() + "&ad_type=" + adType);

            LogAd localLogAd = new LogAd(data.getType(), impTrackings, clickTrackings);
            logAds.add(localLogAd);

            // External tracking tag
            List<String> imp3rdTrackings = new ArrayList<String>();
            String imp3rdTracking = "https://bs.serving-sys.com/serving/adServer.bs?cn=display&c=19&mc=imp&pli=25836608&PluID=0&ord=[timestamp]&rtu=-1";
            imp3rdTracking = imp3rdTracking.replace("[timestamp]", DateTimeUtil.currentUnixTimestamp() + "");
            imp3rdTrackings.add(imp3rdTracking);
            LogAd externalTrackAd = new LogAd(EXTERNAL, imp3rdTrackings, new ArrayList<>());
            logAds.add(externalTrackAd);
            
            sources = new ArrayList<String>(Arrays.asList(sourcesString.split(",")));

            WifiBannerModel model = new WifiBannerModel(placementId, creativeId, width, height, displayType, creativeType, creativeName,
                    landingPage, sources, logAds);
            try {
                return StringEscapeUtils.unescapeJava(model.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return finalResult;
    }

}
