package adsplay.delivery.build.result;

import java.util.Map;

import adsplay.common.model.CreativeData;
import rfx.core.configs.WorkerConfigs;

public interface BuildingResultService {
    
    public static final String LOG_DOMAIN = WorkerConfigs.load().getCustomConfig("log1ServerDomain");

    public static final String LOG_PREFIX = "https://" + LOG_DOMAIN + "/track/banner_ads?";
    
    String buildingResult(CreativeData element, Map<String, Object> paramAsMap, int placementId);
}
